#pragma once

#include <string>
#include <fstream>

namespace tgal {

class ShaderReader {
public:
	virtual bool exists(const char* path) const = 0;
	virtual void open(const char* path) = 0;
	virtual void read(char* data) = 0;
	virtual unsigned length() const = 0;
	virtual void close() = 0;
};

enum class SourceLang {
	GLSL, HLSL, SPIRV
};

class ShaderModule {
public:
	struct CreateInfo {
		SourceLang src_lang;
		std::string path;
		ShaderReader* reader = nullptr;
	};

	virtual ~ShaderModule() {}
};

struct ShaderStage {
	enum Stage {
		Vert = 0x00000001,
		Tesc = 0x00000002,
		Tese = 0x00000004,
		Geom = 0x00000008,
		Frag = 0x00000010,
		Comp = 0x00000020
	};

	ShaderModule* module;
	Stage stage;
	std::string entry;

	ShaderStage(ShaderModule* module, Stage stage, const std::string &entry)
		: module(module), stage(stage), entry(entry) {}
};

// Basic implementation of ShaderReader.
class BasicShaderReader : public ShaderReader {
public:
	virtual bool exists(const char* path) const override;
	virtual void open(const char* path) override;
	virtual void read(char* data) override;
	virtual unsigned length() const override;
	virtual void close() override;

private:
	std::ifstream m_file;
	char* m_content = nullptr;
	int m_length = 0;
};

}
