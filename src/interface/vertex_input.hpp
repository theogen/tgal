#pragma once

#include <vector>

namespace tgal {

struct VertexInput {
	enum class Rate { Vertex, Instance };

	struct BindingDesc {
		uint32_t binding;
		uint32_t stride;
		Rate rate;

		BindingDesc() {}
		BindingDesc(uint32_t binding, uint32_t stride, Rate rate)
			: binding(binding), stride(stride), rate(rate) {}
	};

	struct AttributeDesc {
		uint32_t binding;
		uint32_t location;
		Format format;
		uint32_t offset;

		AttributeDesc() {}
		AttributeDesc(
			uint32_t binding,
			uint32_t loc,
			Format format,
			uint32_t offset
		) :
			binding(binding),
			location(loc),
			format(format),
			offset(offset)
		{}
	};

	std::vector<BindingDesc> bindings;
	std::vector<AttributeDesc> attributes;
};

}
