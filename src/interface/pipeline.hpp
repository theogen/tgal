#pragma once

#include <vector>

#include "types.hpp"
#include "shader.hpp"
#include "swapchain.hpp"
#include "descriptor_set_layout.hpp"
#include "render_pass.hpp"
#include "vertex_input.hpp"

namespace tgal {

enum class PrimitiveTopology {
	PointList,

	LineList,
	LineStrip,

	TriangleList,
	TriangleStrip,
	TriangleFan,

	LineListAdj,
	LineStripAdj,
	TriangleListAdj,
	TriangleStripAdj,

	PatchList
};

struct InputAssembly {
	PrimitiveTopology topology;
	bool primitive_restart;

	InputAssembly() {}
	InputAssembly(PrimitiveTopology topology, bool primitive_restart)
		: topology(topology), primitive_restart(primitive_restart) {}
};

struct Rasterization {

	enum class PolygonMode { Fill, Line, Point };
	enum class CullMode {
		None = 0,
		Back = 0x00000001,
		Front = 0x00000002,
		BackAndFront = 0x00000003
	};

	PolygonMode polygon_mode;
	CullMode cull_mode;
	float line_width = 1.0f;
};

struct PipelineLayout {
	std::vector<DescriptorSetLayout*> layouts;
};

class Pipeline {
public:
	struct CreateInfo {
		// Shader stages.
		std::vector<ShaderStage> stages;
		// Vertex input.
		VertexInput vertex_input;
		// Input assembly.
		InputAssembly input_assembly;
		// Viewport.
		Viewport viewport;
		// Scissors.
		Rect2D scissor;
		// Rasterization.
		Rasterization rasterization;
		// Pipeline layout.
		PipelineLayout layout;
		// Render pass.
		RenderPass* render_pass;

		CreateInfo() {}
	};

	virtual ~Pipeline() {}
};

}
