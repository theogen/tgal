#pragma once

#include "../enum_flag_operators.hpp"
#include "types.hpp"
#include "swapchain.hpp"

namespace tgal {

enum class BufferUsage {
    IndexBuffer,
    VertexBuffer,
    UniformBuffer
};

// Manages a buffer or multiple identical buffers.
class Buffer {
public:
	struct CreateInfo {
		BufferUsage usage;
		MemoryAccess access;
		uint32_t size;
		const void* data = nullptr;
		Swapchain* swapchain = nullptr;
	};

	virtual ~Buffer() {}

	virtual void write(
		void* data,
		uint32_t size,
		uint32_t offset = 0
	) = 0;
};

}
