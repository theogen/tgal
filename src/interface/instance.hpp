#pragma once

#include "../version.hpp"

#include "window.hpp"
#include "physical_device.hpp"

namespace tgal {

class Instance {
public:
	struct CreateInfo {
		// App info.
		const char* application_name;
		Version     application_version;
		const char* engine_name;
		Version     engine_version;

		// Window.
		Window* window;
	};

	virtual ~Instance() {}

	virtual PhysicalDevice* pick_physical_device() = 0;

	virtual Window* window() const = 0;
};

}
