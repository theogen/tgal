#pragma once

#include <vector>
#include "shader.hpp"

namespace tgal {

enum DescriptorType {
    CombinedImageSampler,
    UniformBuffer
};

struct DescriptorSetLayout {

	struct Binding {
		uint32_t binding;
		uint32_t count;
		DescriptorType type;
		ShaderStage::Stage stages;

		Binding() {}
		Binding(
			uint32_t binding,
			uint32_t count,
			DescriptorType type,
			ShaderStage::Stage stages
		) :
			binding(binding),
			count(count),
			type(type),
			stages(stages)
		{}
	};

	struct CreateInfo {
		std::vector<Binding> bindings;
	};

	virtual ~DescriptorSetLayout() {}

	virtual const std::vector<Binding>& bindings() const = 0;
};

}
