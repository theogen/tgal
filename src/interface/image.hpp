#pragma once

#include "types.hpp"
#include "../enum_flag_operators.hpp"

namespace tgal {

class Swapchain;

enum class ImageUsage {
    Sampled = 0x00000004,
    Storage = 0x00000008,
    ColorAttachment = 0x00000010,
    DepthStencilAttachment = 0x00000020,
    TransientAttachment = 0x00000040,
    InputAttachment = 0x00000080,
    ShadingRateImageNV = 0x00000100,
    FragmentDensityMap = 0x00000200
}; DEFINE_ENUM_FLAG_OPERATORS(ImageUsage)

enum class Filter {
	Nearest,
	Linear
};

class Image {
public:
	struct CreateInfo {
		ImageUsage usage;
		MemoryAccess access;
		Format format;
		uint32_t width;
		uint32_t height;
		const void* pixels = nullptr;
		Filter mag_filter = Filter::Linear;
		Filter min_filter = Filter::Linear;
		bool gen_mipmap = false;

		Swapchain* swapchain = nullptr;
	};

	virtual ~Image() {}

	virtual uint32_t width() const = 0;
	virtual uint32_t height() const = 0;
	virtual ImageUsage usage() const = 0;
};

}
