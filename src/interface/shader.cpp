#include "shader.hpp"

#include <sys/stat.h>

namespace tgal {

bool BasicShaderReader::exists(const char* path) const
{
	struct stat buffer;   
	return 0 == stat(path, &buffer); 
}

void BasicShaderReader::open(const char* path)
{
	m_file.open(path, std::ios_base::binary | std::ios_base::ate);
	m_length = (int)m_file.tellg();
}

void BasicShaderReader::read(char* data)
{
	m_file.seekg(0, m_file.beg);
	m_file.read(data, m_length);
}

unsigned BasicShaderReader::length() const
{
	return m_length;
}

void BasicShaderReader::close()
{
	m_file.close();
	m_length = 0;
}

}
