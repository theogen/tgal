#pragma once

#include "device.hpp"

namespace tgal {

class PhysicalDevice {
public:
	virtual ~PhysicalDevice() {}
	virtual Device* create_device() = 0;
	virtual const char* name() const = 0; 
};

}
