#pragma once

#include <vector>
#include "buffer.hpp"
#include "image.hpp"
#include "swapchain.hpp"
#include "descriptor_set_layout.hpp"

namespace tgal {

struct DescriptorBufferInfo {
	Buffer* buffer;
	uint32_t offset;
	uint32_t range;

	DescriptorBufferInfo() {}
	DescriptorBufferInfo(Buffer* buffer, uint32_t offset, uint32_t range)
		: buffer(buffer), offset(offset), range(range) {}
};

struct DescriptorImageInfo {
	Image* image;

	DescriptorImageInfo() {}
	DescriptorImageInfo(Image* image) : image(image) {}
};

class DescriptorWrite {
	DescriptorBufferInfo m_buffer_info;
	DescriptorImageInfo m_image_info;
	bool m_use_buffer = false;
	bool m_use_image = false;

public:
	DescriptorWrite(DescriptorBufferInfo& buffer_info)
		: m_buffer_info(buffer_info), m_use_buffer(true) {}
	DescriptorWrite(Buffer* buffer, uint32_t offset, uint32_t range)
		: m_buffer_info(buffer, offset, range), m_use_buffer(true) {}

	DescriptorWrite(DescriptorImageInfo& image_info)
		: m_image_info(image_info), m_use_image(true) {}
	DescriptorWrite(Image* image)
		: m_image_info(image), m_use_image(true) {}

	DescriptorBufferInfo buffer_info() const noexcept { return m_buffer_info; }
	DescriptorImageInfo image_info() const noexcept { return m_image_info; }
	bool use_buffer() const noexcept { return m_use_buffer; }
	bool use_image() const noexcept { return m_use_image; }
};

struct DescriptorSet {
	struct CreateInfo {
		DescriptorSetLayout* layout;
		std::vector<DescriptorWrite> writes;
		Swapchain* swapchain = nullptr;
	};

	virtual ~DescriptorSet() {}
};

}
