#pragma once

#include <SDL2/SDL.h>

#include "swapchain.hpp"
#include "shader.hpp"
#include "window.hpp"
#include "pipeline.hpp"
#include "command_buffer.hpp"
#include "descriptor_set_layout.hpp"
#include "descriptor_set.hpp"
#include "image.hpp"
#include "framebuffer.hpp"

namespace tgal {

/*! \brief Interface for logical devices.
 * 
 * Logical device represents a subset of physical device with its own features,
 * extensions, queue families and validation layers. It is a context in which
 * rendering pipelines operate.
 *
 * This is an API-agnostic interface.
 */
class Device {
public:
	virtual ~Device() {}

	virtual Swapchain* create_swapchain(
		const Swapchain::CreateInfo& info) = 0;
	
	virtual ShaderModule* create_shader_module(
		const ShaderModule::CreateInfo& info) = 0;

	virtual RenderPass* create_render_pass(
		const RenderPass::CreateInfo& info) = 0;

	virtual Framebuffer* create_framebuffer(
		const Framebuffer::CreateInfo& info) = 0;

	virtual DescriptorSetLayout* create_descriptor_set_layout(
		const DescriptorSetLayout::CreateInfo& info) = 0;

	virtual DescriptorSet* create_descriptor_set(
		const DescriptorSet::CreateInfo& info) = 0;

	virtual Pipeline* create_pipeline(
		const Pipeline::CreateInfo& info) = 0;

	virtual CommandBuffer* create_command_buffer(
		const CommandBuffer::CreateInfo& info) = 0;

	virtual Buffer* create_buffer(
		const Buffer::CreateInfo& info) = 0;

	virtual Image* create_image(
		const Image::CreateInfo& info) = 0;

#if 0
	virtual ImageView* create_image_view(
		const ImageView::CreateInfo& info) = 0;

	virtual Sampler* create_sampler(
		const Sampler::CreateInfo& info) = 0;
#endif

	/* Device methods */

	virtual void wait_idle() = 0;
	virtual void submit_single_time_commands(CommandBuffer* buffer) = 0;

	virtual void queue(CommandBuffer* commands) = 0;

	virtual void clear_queue() = 0;

	virtual void submit() = 0;
};

}
