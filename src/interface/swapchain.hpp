#pragma once

#include "types.hpp"
#include "window.hpp"
#include "image.hpp"
#include "format.hpp"

namespace tgal {

class CommandBuffer;
class RenderPass;

class Swapchain {
public:
	struct CreateInfo {
		Window* window;
	};

	virtual ~Swapchain() {}

	virtual const Image* image() const = 0;
	virtual Extent extent() const = 0;
	virtual uint32_t images_count() const = 0;

	virtual void acquire_next_image() = 0;
	virtual void present() = 0;

	virtual uint32_t current_image_index() const = 0;
};

}
