#pragma once

#include "swapchain.hpp"

namespace tgal {

class RenderPass;

// Represents a single or multiple identical frambuffers.
class Framebuffer {
public:
	struct CreateInfo {
		RenderPass* render_pass;
		uint32_t width, height;
		Swapchain* swapchain = nullptr;
	};

	virtual ~Framebuffer() {}
};

}
