#pragma once

#include <SDL2/SDL.h>

namespace tgal {

class Window {
public:
	struct CreateInfo {
		const char* title;
		int w, h;
		int x, y;
		uint32_t flags = 0;
	};

	virtual ~Window() {}

	virtual SDL_Window* sdl_window() const noexcept = 0;
};

}
