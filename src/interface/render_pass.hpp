#pragma once

#include "types.hpp"
#include "format.hpp"
#include "image.hpp"
#include "swapchain.hpp"

namespace tgal {

class Swapchain;
class Framebuffer;

class RenderPass {
public:
	struct CreateInfo {
		std::vector<const Image*> attachments;
		Swapchain* swapchain = nullptr;
	};

	struct BeginInfo {
		const RenderPass* render_pass;
		const Framebuffer* framebuffer;
		Extent extent;
		Color clear_color;
	};

	virtual ~RenderPass() {}
};

}
