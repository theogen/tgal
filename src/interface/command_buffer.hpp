#pragma once

#include <vector>

#include "pipeline.hpp"
#include "types.hpp"
#include "buffer.hpp"
#include "descriptor_set.hpp"
#include "framebuffer.hpp"
#include "render_pass.hpp"
#include "swapchain.hpp"

namespace tgal {

class Swapchain;

enum class IndexType { UInt16, UInt32 };

class CommandBuffer {
public:
	struct CreateInfo {
		bool one_time_submit = false;
		Swapchain* swapchain = nullptr;
	};

	virtual ~CommandBuffer() {}

	virtual void begin() = 0;
	virtual void end() = 0;

	virtual void begin_render_pass(const RenderPass::BeginInfo& info) = 0;
	virtual void end_render_pass() = 0;

	virtual void bind_pipeline(Pipeline* pipeline) = 0;

	virtual void bind_index_buffer(
		Buffer* index_buffer, IndexType type) = 0;

	virtual void bind_vertex_buffers(
		uint32_t first_binding, uint32_t binding_count, Buffer* buffer) = 0;

	virtual void bind_descriptor_sets(
		uint32_t first_set, size_t count, DescriptorSet* sets) = 0;

	virtual void set_viewport(Viewport viewport) = 0;
	virtual void set_scissor(Rect2D scissor) = 0;

	virtual void draw(
		uint32_t vertex_count,
		uint32_t instance_count,
		uint32_t first_vertex,
		uint32_t first_instance
	) = 0;

	virtual void draw_indexed(
		uint32_t index_count,
		uint32_t instance_count,
		uint32_t first_index,
		uint32_t vertex_offset,
		uint32_t first_instance
	) = 0;
};

}
