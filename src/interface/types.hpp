#pragma once

#include "../enum_flag_operators.hpp"
#include "format.hpp"

namespace tgal {

struct Offset {
	uint32_t x, y;

	Offset() {}
	Offset(uint32_t x, uint32_t y)
		: x(x), y(y) {}
};

struct Extent {
	uint32_t w, h;

	Extent() {}
	Extent(uint32_t w, uint32_t h)
		: w(w), h(h) {}
};

struct Rect2D {
	Offset offset;
	Extent extent;

	Rect2D() {}
	Rect2D(Offset offset, Extent extent)
		: offset(offset), extent(extent) {}
	Rect2D(uint32_t x, uint32_t y, Extent extent)
		: offset(x, y), extent(extent) {}
	Rect2D(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
		: offset(x, y), extent(w, h) {}
};

struct Color {
	float r, g, b, a;

	Color() : r(0.0f), g(0.0f), b(0.0f), a(1.0f) {}
	Color(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}
};

struct Viewport {
	float x, y;
	float w, h;

	Viewport() {}
	Viewport(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
		: x(x), y(y), w(w), h(h) {}
	Viewport(uint32_t x, uint32_t y, Extent extent)
		: x(x), y(y), w(extent.w), h(extent.h) {}
};

enum class MemoryAccess {
	Unknown,
	GpuOnly,
	CpuOnly,
	CpuToGpu,
	GpuToCpu
};

}
