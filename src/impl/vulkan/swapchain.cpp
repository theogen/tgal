#include <limits>

#include "swapchain.hpp"
#include "device.hpp"
#include "render_pass.hpp"
#include "command_buffer.hpp"
#include "image.hpp"

namespace tgal {
namespace vulkan {

Swapchain::Swapchain(
	SwapchainCreator* creator,
	Window* window,
	const VkSwapchainKHR& swapchain,
	const SwapchainSupportDetails& details,
	VkSurfaceFormatKHR surface_format,
	VkPresentModeKHR present_mode,
	VkExtent2D extent
) :
	p_creator(creator),
	p_device(creator->device()),
	m_vk_device(p_device->vk_object()),
	m_present_queue(p_device->present_queue()),
	p_window(window),
	m_swapchain(swapchain),
	m_support_details(details),
	m_surface_format(surface_format),
	m_present_mode(present_mode),
	m_extent(extent),
	m_image(this)
{
	m_image.m_vk_format = m_surface_format.format;

	create_sync_objects();
	create();
}

Swapchain::~Swapchain()
{
	destroy();

	for (size_t i = 0; i < p_device->max_frames_in_flight(); ++i) {
		vkDestroySemaphore(m_vk_device, m_image_available_semaphores[i], nullptr);
	}
}

void Swapchain::create()
{
	// Get swap chain images.
	uint32_t image_count;
	vkGetSwapchainImagesKHR(m_vk_device, m_swapchain, &image_count, nullptr);
	m_image.m_images.resize(image_count);
	vkGetSwapchainImagesKHR(
		m_vk_device, m_swapchain, &image_count, m_image.m_images.data());

	// Creating image views.
	create_image_views();
}

void Swapchain::destroy()
{
	for (auto image_view : m_image.m_image_views) {
		vkDestroyImageView(m_vk_device, image_view, nullptr);
	}

	vkDestroySwapchainKHR(m_vk_device, m_swapchain, nullptr);
}

void Swapchain::recreate()
{
	// Waiting for device to finish everything.
	vkDeviceWaitIdle(m_vk_device);

	// Destroying swapchain.
	destroy();
	// Querying support details.
	m_support_details = SwapchainCreator::query_support(
		p_device->physical_device()->vk_object(), p_window->surface);
	// Choosing new extent.
	m_extent
		= p_creator->choose_extent(p_window, m_support_details.capabilities);
	// Recreating swapchain with the same parameters.
	m_swapchain = p_creator->create_vk_swapchain(
		p_window->surface,
		m_support_details,
		m_surface_format,
		m_present_mode,
		m_extent
	);
	// Re-Initializing this object.
	create();
	// Recreating all dependants.
	for (auto dep : m_dependants)
		dep->recreate(this);
}

void Swapchain::add_dependant(SwapchainDependant* dep)
{
	m_dependants.push_back(dep);
}

void Swapchain::remove_dependant(SwapchainDependant* dep)
{
	size_t i = 0;
	for (; i < m_dependants.size(); ++i)
		if (m_dependants[i] == dep)
			break;
	m_dependants.erase(m_dependants.begin() + i);
}

void Swapchain::create_image_views()
{
	m_image.m_image_views.resize(m_image.m_images.size());

	for (size_t i = 0; i < m_image.m_images.size(); ++i) {
		m_image.m_image_views[i] = p_device->create_image_view(
			m_image.m_images[i],
			m_image.m_vk_format,
			VK_IMAGE_ASPECT_COLOR_BIT,
			1
		);
	}
}
void Swapchain::create_sync_objects()
{
	m_image_available_semaphores.resize(p_device->max_frames_in_flight());

	VkSemaphoreCreateInfo semaphore_info = {};
	semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	for (size_t i = 0; i < p_device->max_frames_in_flight(); ++i) {
		int img_sem_res = vkCreateSemaphore(
			m_vk_device, &semaphore_info,
			nullptr, &m_image_available_semaphores[i]);

		if (img_sem_res != VK_SUCCESS)
			throw std::runtime_error("Failed to create sync objects!");
	}
}

VkSemaphore Swapchain::image_available_semaphore() const noexcept
{
	return m_image_available_semaphores[p_device->current_in_flight_frame()];
}

void Swapchain::acquire_next_image()
{
	// Waiting for fences.
	vkWaitForFences(
		p_device->vk_object(),
		1, p_device->in_flight_fence(),
		VK_TRUE,
		std::numeric_limits<uint64_t>::max()
	);

	// Aquiring swapchain images.
	VkResult result = vkAcquireNextImageKHR(
		p_device->vk_object(),
		m_swapchain,
		std::numeric_limits<uint64_t>::max(),
		m_image_available_semaphores[p_device->current_in_flight_frame()],
		VK_NULL_HANDLE,
		&m_current_image_index
	);

	if (result == VK_ERROR_OUT_OF_DATE_KHR) {
		recreate();
		acquire_next_image();
	}
	else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
		throw std::runtime_error("Failed to acquire swap chain image!");
	}
}

void Swapchain::present()
{
	VkPresentInfoKHR present_info = {};
	present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	present_info.waitSemaphoreCount = 1;
	VkSemaphore signal_semaphores[] = { p_device->render_finished_semaphore() };
	present_info.pWaitSemaphores = signal_semaphores;
	VkSwapchainKHR swap_chains[] = {m_swapchain};
	present_info.swapchainCount = 1;
	present_info.pSwapchains = swap_chains;
	present_info.pImageIndices = &m_current_image_index;
	present_info.pResults = nullptr;

	int result = vkQueuePresentKHR(m_present_queue, &present_info);

	bool need_recreate = false;
	if (result == VK_ERROR_OUT_OF_DATE_KHR ||
		result == VK_SUBOPTIMAL_KHR ||
		m_framebuffer_resized)
	{
		m_framebuffer_resized = false;
		need_recreate = true;
	}
	else if (result != VK_SUCCESS) {
		throw std::runtime_error("Failed to present swap chain image!");
	}

	p_device->increment_current_in_flight_frame();

	if (need_recreate)
		recreate();
}

}}
