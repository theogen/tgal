#include "swapchain_image.hpp"
#include "swapchain.hpp"

namespace tgal {
namespace vulkan {

uint32_t SwapchainImage::width() const noexcept
{
	return p_swapchain->extent().w;
}

uint32_t SwapchainImage::height() const noexcept
{
	return p_swapchain->extent().h;
}

}}
