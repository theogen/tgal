#include <stdexcept>

#include "device.hpp"
#include "shader.hpp"
#include "physical_device.hpp"
#include "render_pass.hpp"
#include "framebuffer.hpp"
#include "pipeline.hpp"
#include "command_buffer.hpp"
#include "buffer.hpp"
#include "image.hpp"

#include "descriptor_set_layout.hpp"
#include "descriptor_set.hpp"

namespace tgal {
namespace vulkan {

Device::Device(
	PhysicalDevice* physical_device_obj,
	VkDevice device,
	VkQueue graphics_queue,
	VkQueue present_queue
) :
	p_physical_device(physical_device_obj),
	m_device(device),
	m_graphics_queue(graphics_queue),
	m_present_queue(present_queue),
	m_swapchain_creator(this)
{
	create_command_pools();

	// Memory allocator.

	VmaAllocatorCreateInfo allocator_info = {};
	allocator_info.physicalDevice = p_physical_device->vk_object();
	allocator_info.device = m_device;

	vmaCreateAllocator(&allocator_info, &m_allocator);

	create_sync_objects();
}

Device::~Device()
{
	for (size_t i = 0; i < m_max_frames_in_flight; ++i) {
		vkDestroySemaphore(m_device, m_render_finished_semaphores[i], nullptr);
		vkDestroyFence(m_device, m_in_flight_fences[i], nullptr);
	}

	vmaDestroyAllocator(m_allocator);
	vkDestroyCommandPool(m_device, m_graphics_command_pool, nullptr);
	vkDestroyDevice(m_device, nullptr);
}

void Device::create_sync_objects()
{
	m_render_finished_semaphores.resize(m_max_frames_in_flight);
	m_in_flight_fences.resize(m_max_frames_in_flight);

	VkSemaphoreCreateInfo semaphore_info = {};
	semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fence_info = {};
	fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for (size_t i = 0; i < m_max_frames_in_flight; ++i) {
		int render_sem_res = vkCreateSemaphore(
			m_device, &semaphore_info,
			nullptr, &m_render_finished_semaphores[i]);

		int fence_res = vkCreateFence(
			m_device, &fence_info,
			nullptr, &m_in_flight_fences[i]);

		if (render_sem_res != VK_SUCCESS ||
			fence_res      != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create sync objects!");
		}
	}
}


/* Interface */

tgal::Swapchain* Device::create_swapchain(const Swapchain::CreateInfo& info)
{
	return m_swapchain_creator.create_swapchain(static_cast<Window*>(info.window));
}

tgal::ShaderModule* Device::create_shader_module(
	const ShaderModule::CreateInfo& info)
{
	return new ShaderModule(this, info);
}

tgal::RenderPass* Device::create_render_pass(const RenderPass::CreateInfo& info)
{
	return new RenderPass(this, info);
}

tgal::Framebuffer* Device::create_framebuffer(const Framebuffer::CreateInfo& info)
{
	return new Framebuffer(this, info);
}

tgal::Pipeline* Device::create_pipeline(const Pipeline::CreateInfo& info)
{
	return new Pipeline(this, info);
}

tgal::CommandBuffer*
Device::create_command_buffer(const CommandBuffer::CreateInfo& info)
{
	return new CommandBuffer(this, info, m_graphics_command_pool);
}

CommandBuffer* Device::begin_single_time_commands()
{
	CommandBuffer::CreateInfo info;
	info.one_time_submit = true;

	return new CommandBuffer(this, info, m_graphics_command_pool);
}

void Device::submit_single_time_commands(tgal::CommandBuffer* buffer)
{
	VkSubmitInfo submit_info = {};
	submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submit_info.commandBufferCount = 1;
	VkCommandBuffer vk_cmd_buffer = static_cast<CommandBuffer*>(buffer)->vk_object();
	submit_info.pCommandBuffers = &vk_cmd_buffer;

	vkQueueSubmit(m_graphics_queue, 1, &submit_info, VK_NULL_HANDLE);
	vkQueueWaitIdle(m_graphics_queue);
}

tgal::Buffer* Device::create_buffer(const Buffer::CreateInfo& info)
{
	return new Buffer(this, info);
}

tgal::Image* Device::create_image(const Image::CreateInfo& info)
{
	return new Image(this, info);
}

tgal::DescriptorSetLayout* Device::create_descriptor_set_layout(
	const DescriptorSetLayout::CreateInfo& info)
{
	return new DescriptorSetLayout(this, info);
}

tgal::DescriptorSet* Device::create_descriptor_set(
	const DescriptorSet::CreateInfo& info)
{
	return new DescriptorSet(this, info);
}

void Device::queue(tgal::CommandBuffer* commands)
{
	auto vk_commands = static_cast<CommandBuffer*>(commands);
	auto swapchain = vk_commands->swapchain();
	VkCommandBuffer vk_buffer = vk_commands->vk_object(swapchain->current_image_index());
	m_queued_command_buffers.push_back(vk_buffer);
	p_queued_command_buffers_swapchain = swapchain;
}

void Device::clear_queue()
{
	m_queued_command_buffers.clear();
}

void Device::submit()
{
	Swapchain* swapchain = p_queued_command_buffers_swapchain;
	VkSubmitInfo submit_info = {};
	submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	VkSemaphore wait_semaphores[] = { swapchain->image_available_semaphore() };
	VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
	submit_info.waitSemaphoreCount = 1;
	submit_info.pWaitSemaphores = wait_semaphores;
	submit_info.pWaitDstStageMask = waitStages;
	submit_info.commandBufferCount = m_queued_command_buffers.size();
	submit_info.pCommandBuffers = m_queued_command_buffers.data();
	VkSemaphore signal_semaphores[] = {m_render_finished_semaphores[m_current_in_flight_frame]};
	submit_info.signalSemaphoreCount = 1;
	submit_info.pSignalSemaphores = signal_semaphores;

	vkResetFences(m_device, 1, &m_in_flight_fences[m_current_in_flight_frame]);

	int res = vkQueueSubmit(
		m_graphics_queue,
		1, &submit_info,
		m_in_flight_fences[m_current_in_flight_frame]
	);

	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to submit draw command buffer!");
	}
}

/* Public methods */

VkImageView Device::create_image_view(
	VkImage image,
	VkFormat format,
	VkImageAspectFlags aspect_mask,
	uint32_t mip_levels
)
{
	// Create info.
	VkImageViewCreateInfo view_info = {};
	view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	view_info.image = image;
	view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
	view_info.format = format;
	view_info.subresourceRange.aspectMask = aspect_mask;
	view_info.subresourceRange.baseMipLevel = 0;
	view_info.subresourceRange.levelCount = mip_levels;
	view_info.subresourceRange.baseArrayLayer = 0;
	view_info.subresourceRange.layerCount = 1;

	// Creating the image view.
	VkImageView image_view;
	int res = vkCreateImageView(m_device, &view_info, nullptr, &image_view);
	if (res != VK_SUCCESS) {
		throw std::runtime_error("failed to create texture image view!");
	}

	return image_view;
}

/* Private methods */

void Device::create_command_pools()
{
	QueueFamilyIndices families = p_physical_device->queue_families();
	VkCommandPoolCreateInfo pool_info = {};
	pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	pool_info.queueFamilyIndex = families.graphics;
	pool_info.flags = 0;

	int res = vkCreateCommandPool(
		m_device,
		&pool_info,
		nullptr,
		&m_graphics_command_pool
	);

	if (res != VK_SUCCESS) {
		throw std::runtime_error("failed to create command pool!");
	}
}

}}
