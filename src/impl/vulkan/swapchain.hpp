#pragma once

#include <vector>
#include <vulkan/vulkan.hpp>

#include "interface/swapchain.hpp"
#include "swapchain_image.hpp"
#include "swapchain_dependant.hpp"
#include "window.hpp"

namespace tgal {
namespace vulkan {

class Device;
class SwapchainCreator;
class SwapchainDependant;

struct SwapchainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> present_modes;
};

class Swapchain : public tgal::Swapchain {
public:
	Swapchain(
		SwapchainCreator* creator,
		Window* window,
		const VkSwapchainKHR& swapchain,
		const SwapchainSupportDetails& details,
		VkSurfaceFormatKHR surface_format,
		VkPresentModeKHR present_mode,
		VkExtent2D extent
	);

	~Swapchain();

	void create();
	void destroy();

	/* Rendering. */

	virtual void acquire_next_image() override;
	virtual void present() override;

	virtual uint32_t current_image_index() const override
	{ return m_current_image_index; };

	/* Dependants */

	void add_dependant(SwapchainDependant* dep);

	void remove_dependant(SwapchainDependant* dep);

	/* Getters */

	virtual const tgal::Image* image() const override
	{ return &m_image; }
	virtual Extent extent() const override
	{ return Extent(m_extent.width, m_extent.height); }
	virtual uint32_t images_count() const override
	{ return m_image.m_images.size(); }

	VkFormat vk_image_format() const { return m_image.m_vk_format; }
	VkExtent2D vk_extent() const { return m_extent; }

	VkSemaphore image_available_semaphore() const noexcept;

private:
	void create_image_views();
	void create_sync_objects();

	void recreate();

private:
	/* Device objects */

	SwapchainCreator* p_creator;

	Device* p_device;
	VkDevice m_vk_device;

	VkQueue m_present_queue;

	/* Swapchain objects */

	Window* p_window;

	VkSwapchainKHR m_swapchain;

	SwapchainSupportDetails m_support_details;
	VkSurfaceFormatKHR m_surface_format;
	VkPresentModeKHR m_present_mode;

	VkExtent2D m_extent;
	SwapchainImage m_image;

	uint32_t m_current_image_index = 0;

	/* Sync objects */

	std::vector<VkSemaphore> m_image_available_semaphores;

	/* Resize */

	bool m_framebuffer_resized = false;

	/* Dependants */
	std::vector<SwapchainDependant*> m_dependants;
};

}}
