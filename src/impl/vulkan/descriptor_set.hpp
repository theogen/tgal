#pragma once

#include <vector>
#include <vulkan/vulkan.h>
#include "interface/descriptor_set.hpp"
#include "swapchain.hpp"

namespace tgal {
namespace vulkan {

class DescriptorPool;
class Device;

class DescriptorSet : public tgal::DescriptorSet, public SwapchainDependant {
public:
	DescriptorSet(
		Device* device,
		const CreateInfo& info
	);
	~DescriptorSet();

	VkDescriptorSet vk_object(uint32_t index) const { return m_sets[index]; }

	virtual void recreate(Swapchain* swapchain) override;

private:
	void create(const CreateInfo& info);
	void create_descriptor_pool(const CreateInfo& info);
	void create_descriptor_set(const CreateInfo& info);
	void create_writes(const CreateInfo& info, const VkDescriptorSet& set, uint32_t buffer_index);

	void destroy();

private:
	CreateInfo m_info;
	Device* p_device;
	VkDevice m_vk_device;
	tgal::DescriptorSetLayout* m_layout;
	VkDescriptorPool m_descriptor_pool;
	std::vector<VkDescriptorSet> m_sets;
	uint32_t m_count;
};

}}
