#pragma once

#include <vector>

#include <vulkan/vulkan.hpp>

#include "physical_device.hpp"

namespace tgal {
namespace vulkan {

class Instance;

class PhysicalDeviceSelector {
public:
	PhysicalDeviceSelector(Instance* instance);

	PhysicalDevice* pick_device();
	QueueFamilyIndices find_queue_families(const VkPhysicalDevice& physical_device);
	std::vector<const char*> get_extensions() { return device_extensions; }
	std::vector<const char*> get_validation_layers() { return m_validation_layers; }

private:
	bool is_device_suitable(const VkPhysicalDevice& device);
	bool check_extension_support(const VkPhysicalDevice& device);
	VkSampleCountFlagBits get_max_usable_sample_count(const VkPhysicalDevice& device);

private:
	VkInstance m_instance;
	VkSurfaceKHR m_surface;
	std::vector<const char*> m_validation_layers;

	const std::vector<const char*> device_extensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

};

}}
