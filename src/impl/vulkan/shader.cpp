#include <iostream>
#include <SPIRV/GlslangToSpv.h>
#include "includer.hpp"

#include "shader.hpp"

namespace tgal {
namespace vulkan {

bool ShaderModule::glslang_initialized = false;

ShaderModule::ShaderModule(Device* device, const CreateInfo& info) :
	p_device(device)
{
	auto spirv = glsl_to_spirv(info.reader, info.path);
	create_module(spirv);
}

ShaderModule::~ShaderModule()
{
	vkDestroyShaderModule(p_device->vk_object(), m_shader_module, nullptr);
}

void ShaderModule::create_module(const std::vector<uint32_t>& spirv)
{
	VkShaderModuleCreateInfo create_info = {};
	create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	create_info.codeSize = spirv.size() * sizeof(uint32_t);
	create_info.pCode = spirv.data();

	int res = vkCreateShaderModule(
		p_device->vk_object(), &create_info, nullptr, &m_shader_module);

	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to create shader module!");
	}
}

static std::string dirname(const std::string& str)
{
	size_t found = str.find_last_of("/\\");
	return str.substr(0, found);
}

static std::string suffix(const std::string& name)
{
	const size_t pos = name.rfind('.');
	return (pos == std::string::npos) ? "" : name.substr(pos + 1);
}

EShLanguage ShaderModule::get_shader_stage(const std::string& stage) const
{
	if (stage == "vert")
		return EShLangVertex;
	else if (stage == "tesc")
		return EShLangTessControl;
	else if (stage == "tese")
		return EShLangTessEvaluation;
	else if (stage == "geom")
		return EShLangGeometry;
	else if (stage == "frag")
		return EShLangFragment;
	else if (stage == "comp")
		return EShLangCompute;

	return EShLangCount;
}

VkShaderStageFlagBits ShaderModule::vk_shader_stage(ShaderStage::Stage stage)
{
	if (stage == ShaderStage::Comp)
		return VK_SHADER_STAGE_COMPUTE_BIT;
	else if (stage == ShaderStage::Vert)
		return VK_SHADER_STAGE_VERTEX_BIT;
	else if (stage == ShaderStage::Tesc)
		return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
	else if (stage == ShaderStage::Tese)
		return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
	else if (stage == ShaderStage::Geom)
		return VK_SHADER_STAGE_GEOMETRY_BIT;
	else if (stage == ShaderStage::Frag)
		return VK_SHADER_STAGE_FRAGMENT_BIT;

	return VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM;
}

std::vector<uint32_t> ShaderModule::glsl_to_spirv(ShaderReader* reader, const std::string& path) const
{
	// Initialize glslang.
	if (!glslang_initialized)
	{
		glslang::InitializeProcess();
		glslang_initialized = true;
	}

	std::cout << "Converting `" + path + "' to SPIR-V.\n\n";

	// Getting source.
	reader->open(path.c_str());
	char* source = new char[reader->length() + 1];
	reader->read(source);
	source[reader->length()] = '\0';
	reader->close();

	EShLanguage shader_type = get_shader_stage(suffix(path));
	glslang::TShader shader(shader_type);
	shader.setStrings(&source, 1);

	// Set up Vulkan/Spir-V environment.
	int client_input_semantics_version = 100; // maps to, say, #define VULKAN 100
	glslang::EShTargetClientVersion vulkan_client_version = glslang::EShTargetVulkan_1_0;  // would map to, say, Vulkan 1.0
	glslang::EShTargetLanguageVersion target_version = glslang::EShTargetSpv_1_0;	// maps to, say, SPIR-V 1.0

	shader.setEnvInput(glslang::EShSourceGlsl, shader_type, glslang::EShClientVulkan, client_input_semantics_version);
	shader.setEnvClient(glslang::EShClientVulkan, vulkan_client_version);
	shader.setEnvTarget(glslang::EShTargetSpv, target_version);

	TBuiltInResource resources = default_tbuiltin_resource;
	EShMessages messages = (EShMessages) (EShMsgSpvRules | EShMsgVulkanRules);

	const int default_version = 100;

	DirStackFileIncluder includer(reader);
	
	// Get directory.
	std::string dir = dirname(path);
	includer.push_extern_local_dir(dir);

	std::string preprocessed_glsl;

	if (!shader.preprocess(&resources, default_version, ENoProfile, false, false, messages, &preprocessed_glsl, includer)) 
	{
		std::cout << "GLSL Preprocessing Failed for: " << path << std::endl;
		std::cout << shader.getInfoLog() << std::endl;
		std::cout << shader.getInfoDebugLog() << std::endl;
	}

	const char* preprocessed_cstr = preprocessed_glsl.c_str();
	shader.setStrings(&preprocessed_cstr, 1);

	if (!shader.parse(&resources, 100, false, messages))
	{
		std::cout << "GLSL Parsing Failed for: " << path << std::endl;
		std::cout << shader.getInfoLog() << std::endl;
		std::cout << shader.getInfoDebugLog() << std::endl;
	}

	glslang::TProgram program;
	program.addShader(&shader);

	if(!program.link(messages))
	{
		std::cout << "GLSL Linking Failed for: " << path << std::endl;
		std::cout << shader.getInfoLog() << std::endl;
		std::cout << shader.getInfoDebugLog() << std::endl;
	}

	// if (!Program.mapIO())
	// {
	// 	std::cout << "GLSL Linking (Mapping IO) Failed for: " << path << std::endl;
	// 	std::cout << Shader.getInfoLog() << std::endl;
	// 	std::cout << Shader.getInfoDebugLog() << std::endl;
	// }

	std::vector<unsigned int> spirv;
	spv::SpvBuildLogger logger;
	glslang::SpvOptions spv_options;
	glslang::GlslangToSpv(*program.getIntermediate(shader_type), spirv, &logger, &spv_options);

	// Output the resulting binary.
	//glslang::OutputSpvBin(spirv, (path + ".spv").c_str());

	if (logger.getAllMessages().length() > 0)
	{
		std::cout << logger.getAllMessages() << std::endl;
	}

	//glslang::FinalizeProcess();

	delete [] source;
	return spirv;
}

}}
