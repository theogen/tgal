#pragma once

#include <vulkan/vulkan.h>
#include "vk_mem_alloc.h"

#include "interface/types.hpp"

namespace tgal {
namespace vulkan {
namespace utils {

VkFormat get_vk_format(Format format) noexcept;

VmaMemoryUsage get_vma_memory_usage(MemoryAccess access) noexcept;

}}}
