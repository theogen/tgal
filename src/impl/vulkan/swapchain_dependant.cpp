#include "swapchain_dependant.hpp"
#include "swapchain.hpp"

namespace tgal {
namespace vulkan {

SwapchainDependant::SwapchainDependant(tgal::Swapchain* swapchain)
	: p_swapchain(dynamic_cast<Swapchain*>(swapchain))
{
	if (p_swapchain != nullptr)
		p_swapchain->add_dependant(this);
}

SwapchainDependant::~SwapchainDependant()
{
	if (p_swapchain != nullptr)
		p_swapchain->remove_dependant(this);
}

void SwapchainDependant::set_swapchain(Swapchain* swapchain)
{
	if (p_swapchain != nullptr && swapchain == nullptr)
		p_swapchain->remove_dependant(this);
	p_swapchain = swapchain;
	if (p_swapchain != nullptr)
		p_swapchain->add_dependant(this);
}

}}
