#include <cstring>
#include <iostream>
#include <regex>

#include "debug_messenger.hpp"

namespace tgal {
namespace vulkan {

VkResult CreateDebugUtilsMessengerEXT(
	VkInstance instance,
	const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
	const VkAllocationCallbacks* pAllocator,
	VkDebugUtilsMessengerEXT* pDebugMessenger
) {
	auto func =
		(PFN_vkCreateDebugUtilsMessengerEXT)
		vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
	if (func != nullptr)
		return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
	else
		return VK_ERROR_EXTENSION_NOT_PRESENT;
}

void DestroyDebugUtilsMessengerEXT(
	VkInstance instance,
	VkDebugUtilsMessengerEXT debugMessenger,
	const VkAllocationCallbacks* pAllocator
) {
    auto func =
		(PFN_vkDestroyDebugUtilsMessengerEXT)
		vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");

    if (func != nullptr) {
        func(instance, debugMessenger, pAllocator);
    }
}

static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
	VkDebugUtilsMessageSeverityFlagBitsEXT severity,
	VkDebugUtilsMessageTypeFlagsEXT type,
	const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
	void* user_data
) {
	auto messenger = static_cast<DebugMessenger*>(user_data);
	messenger->callback(
		static_cast<MessageSeverity>(severity),
		static_cast<MessageType>(type),
		callback_data
	);

	return VK_FALSE;
}


DebugMessenger::DebugMessenger(
	const VkInstance& instance,
	const CreateInfo& info
) :
	m_instance(instance),
	m_no_duplicates(info.no_duplicates),
	m_mute(info.mute),
	m_replacements(info.replacements)
{
	VkDebugUtilsMessengerCreateInfoEXT create_info = {};
	create_info.sType
		= VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	create_info.messageSeverity
		= static_cast<VkDebugUtilsMessageSeverityFlagBitsEXT>(info.severity);
	create_info.messageType
		= static_cast<VkDebugUtilsMessageTypeFlagBitsEXT>(info.type);
	create_info.pfnUserCallback = debug_callback;
	create_info.pUserData = this;

	if (CreateDebugUtilsMessengerEXT(
			m_instance,
			&create_info,
			nullptr,
			&m_debug_messenger) != VK_SUCCESS
	) {
			throw std::runtime_error("Failed to set up debug messenger!");
	}
}

DebugMessenger::~DebugMessenger()
{
	DestroyDebugUtilsMessengerEXT(m_instance, m_debug_messenger, nullptr);
}


void DebugMessenger::callback(
	MessageSeverity severity,
	MessageType type,
	const VkDebugUtilsMessengerCallbackDataEXT* callback_data
)
{
	if (m_mute)
		return;

	const char* message = callback_data->pMessage;

	if (m_no_duplicates && 0 == strcmp(message, m_last_message.c_str())) {
		++m_duplicates_count;
	}
	else {
		m_duplicates_count = 1;
		m_last_message = message;
	}

	int type_index = type_name_index(type);
	int severity_index = severity_color_index(severity);

	if (m_duplicates_count > 1) {
		return;
	}

	std::string result_message = replace(message);
	if (result_message.length() == 0)
		return;

	std::cerr
		<< "["
		<< m_severity_colors[severity_index]
		<< m_type_names[type_index]
		<< "\e[39m] "
		<< result_message;

	std::cerr << "\n\n";
}

std::string DebugMessenger::replace(const char* message)
{
	std::string orig_message(message);
	std::string result_message;
	if (m_replacements.size() == 0)
		result_message = orig_message;

	bool none_matched = true;
	for (auto& replace : m_replacements) {
		std::regex rex(replace[0]);

		if (!std::regex_search(orig_message.c_str(), rex))
			continue;

		std::regex_replace(
			std::back_inserter(result_message),
			orig_message.begin(), orig_message.end(),
			rex, replace[1]
		);

		orig_message = result_message;
		none_matched = false;
	}

	if (none_matched)
		result_message = orig_message;

	return result_message;
}

int DebugMessenger::type_name_index(MessageType type)
{
	switch (type) {
		case MessageType::General:
			return 0;
		case MessageType::Validation:
			return 1;
		case MessageType::Performance:
			return 2;
	}

	return -1;
}

int DebugMessenger::severity_color_index(MessageSeverity severity)
{
	switch(severity) {
		case MessageSeverity::Verbose:
			return 0;
		case MessageSeverity::Info:
			return 1;
		case MessageSeverity::Warning:
			return 2;
		case MessageSeverity::Error:
			return 3;
	}

	return -1;
}

}} // Namespace
