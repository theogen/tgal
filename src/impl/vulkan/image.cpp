#include <stdexcept>
#include <algorithm>

#include "device.hpp"
#include "image.hpp"
#include "buffer.hpp"
#include "command_buffer.hpp"
#include "utils.hpp"
#include "swapchain.hpp"

namespace tgal {
namespace vulkan {

ImageImpl::ImageImpl(Swapchain* swapchain)
	: SwapchainDependant(static_cast<tgal::Swapchain*>(swapchain))
{}

Image::Image(Device* device, const CreateInfo& info) :
	ImageImpl(dynamic_cast<Swapchain*>(info.swapchain)),
	m_info(info),
	p_device(device),
	m_vk_device(device->vk_object()),
	m_allocator(device->allocator()),
	m_usage(info.usage),
	m_format(utils::get_vk_format(info.format))
{
	create();
}

Image::~Image()
{
	destroy();
}


void Image::create()
{
	if (m_info.swapchain != nullptr)
	{
		m_info.width = m_info.swapchain->extent().w;
		m_info.height = m_info.swapchain->extent().h;
	}

	// Getting usage bits.
	uint32_t usage = get_vk_usage(m_info.usage);
	if (need_staging_buffer(m_info)) {
		usage |=
			VK_BUFFER_USAGE_TRANSFER_DST_BIT |
			VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
	}

	unsigned mip_levels = 1;
	if (m_info.gen_mipmap)
	{
		mip_levels = calc_mip_levels(m_info.width, m_info.height);
	}

	// Creating the image.
	create_image(
		m_info.width, m_info.height,
		m_format,
		static_cast<VkImageUsageFlagBits>(usage),
		m_info.access,
		mip_levels,
		&m_image, &m_allocation
	);

	// Depth image.
	if (m_info.usage == ImageUsage::DepthStencilAttachment) {
		transition_image_layout(
			m_image, m_format, 
			VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			1
		);
	}
	// Texture image.
	else if (m_info.usage == ImageUsage::Sampled &&
			need_staging_buffer(m_info))
	{
		create_texture_image(
			m_info.pixels, m_info.width, m_info.height, mip_levels);
	}

	// Choosing aspect mask.
	VkImageAspectFlags aspect_mask = VK_IMAGE_ASPECT_COLOR_BIT;
	if (m_info.usage == ImageUsage::DepthStencilAttachment)
		aspect_mask = VK_IMAGE_ASPECT_DEPTH_BIT;
	// Creating image view.
	m_view = p_device->create_image_view(m_image, m_format, aspect_mask, mip_levels);

	// Creating sampler.
	create_sampler(m_info);
}

void Image::create_texture_image(
	const void* data,
	uint32_t width,
	uint32_t height,
	uint32_t mip_levels)
{
	// Staging buffer.
	VkBuffer staging_buffer;
	VmaAllocation staging_buffer_alloc;
	// Image size in bytes: w * h * 4.
	uint32_t image_size = width * height * sizeof(uint32_t);

	Buffer::create_staging_buffer(
		p_device, 
		image_size, data,
		&staging_buffer, &staging_buffer_alloc
	);

	transition_image_layout(
		m_image,
		m_format,
		VK_IMAGE_LAYOUT_UNDEFINED,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		mip_levels
	);
	copy_buffer_to_image(
		staging_buffer, m_image,
		width, height
	);
	generate_mipmaps(
		m_image, m_format,
		width, height,
		mip_levels
	);

	vmaDestroyBuffer(m_allocator, staging_buffer, staging_buffer_alloc);
}

void Image::recreate(Swapchain* swapchain)
{
	// Don't recreate if extent if the same.
	if (swapchain->extent().w == m_info.width &&
		swapchain->extent().h == m_info.height)
	{
		return;
	}

	destroy();
	create();
}

void Image::destroy()
{
	vkDestroySampler(m_vk_device, m_sampler, nullptr);
	vkDestroyImageView(m_vk_device, m_view, nullptr);
	vmaDestroyImage(m_allocator, m_image, m_allocation);
}

void Image::create_image(
	uint32_t width, uint32_t height,
	VkFormat format,
	VkImageUsageFlagBits usage,
	MemoryAccess access,
	uint32_t mip_levels,
	VkImage* image, VmaAllocation* alloc)
{
	VkImageCreateInfo image_info = {};
	image_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	image_info.imageType = VK_IMAGE_TYPE_2D;
	image_info.extent.width = width;
	image_info.extent.height = height;
	image_info.extent.depth = 1;
	image_info.mipLevels = mip_levels;
	image_info.arrayLayers = 1;
	image_info.format = format;
	image_info.tiling = VK_IMAGE_TILING_OPTIMAL;
	image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	image_info.usage = usage;
	image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	image_info.samples = VK_SAMPLE_COUNT_1_BIT;

	VmaAllocationCreateInfo alloc_info = {};
	alloc_info.usage = utils::get_vma_memory_usage(access);

	vmaCreateImage(
		m_allocator,
		&image_info,
		&alloc_info,
		image,
		alloc,
		nullptr
	);
}

void Image::transition_image_layout(
	const VkImage& image, VkFormat format,
	VkImageLayout old_layout, VkImageLayout new_layout,
	uint32_t mip_levels)
{
	CommandBuffer* single_time_cmd = p_device->begin_single_time_commands();
	single_time_cmd->begin();

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.oldLayout = old_layout;
	barrier.newLayout = new_layout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;

	// Determening the aspect mask.
	if (new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		if (has_stencil_component(format)) {
			barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
		}
	}
	else {
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}

	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = mip_levels;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;

	VkPipelineStageFlags source_stage;
	VkPipelineStageFlags destination_stage;

	// Determining access masks and pipeline stages.

	if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED &&
		new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
	{
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

		source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destination_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	}
	else if (old_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
			new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
	{
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		source_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	}
	else if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED &&
			new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
	{
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask =
			VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
			VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

		source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destination_stage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	}
	else {
		throw std::invalid_argument("Unsupported layout transition!");
	}

	// Creating the barrier.
	vkCmdPipelineBarrier(
		single_time_cmd->vk_object(),
		source_stage, destination_stage,
		0,
		0, nullptr,
		0, nullptr,
		1, &barrier
	);

	single_time_cmd->end();
	p_device->submit_single_time_commands(single_time_cmd);
	delete single_time_cmd;
}

void Image::copy_buffer_to_image(
	VkBuffer buffer, VkImage image, uint32_t width, uint32_t height)
{
	CommandBuffer* single_time_cmd = p_device->begin_single_time_commands();
	single_time_cmd->begin();

	VkBufferImageCopy region = {};
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.bufferImageHeight = 0;

	region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	region.imageSubresource.mipLevel = 0;
	region.imageSubresource.baseArrayLayer = 0;
	region.imageSubresource.layerCount = 1;

	region.imageOffset = {0, 0, 0};
	region.imageExtent = {
		width,
		height,
		1
	};

	vkCmdCopyBufferToImage(
		single_time_cmd->vk_object(),
		buffer,
		image,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		1,
		&region
	);

	single_time_cmd->end();
	p_device->submit_single_time_commands(single_time_cmd);
	delete single_time_cmd;
}

bool Image::need_staging_buffer(const CreateInfo& info)
{
	return info.access != MemoryAccess::CpuOnly;
}

bool Image::has_stencil_component(VkFormat format)
{
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT ||
	       format == VK_FORMAT_D24_UNORM_S8_UINT;
}

VkImageUsageFlagBits Image::get_vk_usage(ImageUsage usage) const noexcept
{
	return static_cast<VkImageUsageFlagBits>(usage);
}

void Image::create_sampler(const CreateInfo& info)
{
	VkSamplerCreateInfo sampler_info = {};
	sampler_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	sampler_info.magFilter = get_vk_filter(info.mag_filter);
	sampler_info.minFilter = get_vk_filter(info.min_filter);
	sampler_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	sampler_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	sampler_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	sampler_info.anisotropyEnable = VK_TRUE;
	sampler_info.maxAnisotropy = 16;
	sampler_info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	sampler_info.unnormalizedCoordinates = VK_FALSE;
	sampler_info.compareEnable = VK_FALSE;
	sampler_info.compareOp = VK_COMPARE_OP_ALWAYS;
	sampler_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	sampler_info.mipLodBias = 0;
	sampler_info.minLod = 0;
	sampler_info.maxLod = calc_mip_levels(info.width, info.height);

	int res = vkCreateSampler(
		m_vk_device, &sampler_info, nullptr, &m_sampler);
	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to create texture sampler!");
	}
}

void Image::generate_mipmaps(
	VkImage& image, VkFormat format,
	uint32_t w, uint32_t h, uint32_t mip_levels)
{
	// Check if image format supports linear blitting.
    VkFormatProperties format_properties;
    vkGetPhysicalDeviceFormatProperties(
		p_device->physical_device()->vk_object(), format, &format_properties);

	if (!(format_properties.optimalTilingFeatures &
		VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT))
	{
		throw std::runtime_error("Texture image format does not support linear blitting!");
	}

	CommandBuffer* single_time_cmd = p_device->begin_single_time_commands();
	single_time_cmd->begin();

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.image = image;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;
	barrier.subresourceRange.levelCount = 1;

	int32_t mip_width = w;
	int32_t mip_height = h;

	for (uint32_t i = 1; i < mip_levels; i++) {
		barrier.subresourceRange.baseMipLevel = i - 1;
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

		vkCmdPipelineBarrier(
			single_time_cmd->vk_object(),
			VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
			0, nullptr,
			0, nullptr,
			1, &barrier
		);

		VkImageBlit blit = {};
		blit.srcOffsets[0] = { 0, 0, 0 };
		blit.srcOffsets[1] = { mip_width, mip_height, 1 };
		blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		blit.srcSubresource.mipLevel = i - 1;
		blit.srcSubresource.baseArrayLayer = 0;
		blit.srcSubresource.layerCount = 1;
		blit.dstOffsets[0] = { 0, 0, 0 };
		blit.dstOffsets[1] = {
			mip_width > 1 ? mip_width / 2 : 1,
			mip_height > 1 ? mip_height / 2 : 1, 1
		};
		blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		blit.dstSubresource.mipLevel = i;
		blit.dstSubresource.baseArrayLayer = 0;
		blit.dstSubresource.layerCount = 1;

		vkCmdBlitImage(single_time_cmd->vk_object(),
			image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1, &blit,
			VK_FILTER_LINEAR
		);

		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		vkCmdPipelineBarrier(single_time_cmd->vk_object(),
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
			0, nullptr,
			0, nullptr,
			1, &barrier
		);

		if (mip_width > 1) mip_width /= 2;
		if (mip_height > 1) mip_height /= 2;
	}

	barrier.subresourceRange.baseMipLevel = mip_levels - 1;
    barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

	vkCmdPipelineBarrier(single_time_cmd->vk_object(),
	    VK_PIPELINE_STAGE_TRANSFER_BIT,
		VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
	    0, nullptr,
	    0, nullptr,
	    1, &barrier
	);

	single_time_cmd->end();
	p_device->submit_single_time_commands(single_time_cmd);
	delete single_time_cmd;
}

VkFilter Image::get_vk_filter(Filter filter)
{
	switch (filter) {
		case Filter::Nearest:
			return VK_FILTER_NEAREST;
		case Filter::Linear:
			return VK_FILTER_LINEAR;
	}

	throw std::runtime_error("Unknown filter specified.");
}

unsigned Image::calc_mip_levels(unsigned width, unsigned height)
{
	return std::floor(std::log2(std::max(width, height))) + 1;
}

}}
