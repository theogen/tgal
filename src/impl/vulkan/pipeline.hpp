#pragma once

#include <vulkan/vulkan.hpp>
#include "interface/pipeline.hpp"
#include "device.hpp"

namespace tgal {
namespace vulkan {

class Pipeline : public tgal::Pipeline {
public:
	Pipeline(Device* device, CreateInfo info);
	~Pipeline();

	VkPipeline vk_object() const { return m_pipeline; };

	static VkPrimitiveTopology get_vk_topology(PrimitiveTopology topology);

	VkPipelineLayout pipeline_layout() const { return m_layout; }

private:
	std::vector<VkPipelineShaderStageCreateInfo>
		create_shader_stage_infos(const CreateInfo& info);

	VkPipelineVertexInputStateCreateInfo create_vertex_input_info(
		const CreateInfo& info,
		std::vector<VkVertexInputBindingDescription>& bindings,
		std::vector<VkVertexInputAttributeDescription>& attributes
	);

	VkPipelineInputAssemblyStateCreateInfo
		create_input_assembly_info(const CreateInfo& info);

	VkPipelineViewportStateCreateInfo create_viewport_info(
		const CreateInfo& info, VkViewport& viewport, VkRect2D& scissor);

	VkPipelineRasterizationStateCreateInfo
		create_rasterization_info(const CreateInfo& info);

	VkPipelineMultisampleStateCreateInfo
		create_multisample_info(const CreateInfo& info);

	VkPipelineColorBlendStateCreateInfo create_color_blend_info(
		const CreateInfo& info,
		VkPipelineColorBlendAttachmentState& color_blend_attachment
	);

	VkPipelineLayout create_pipeline_layout(const CreateInfo& info);

	VkPipelineDepthStencilStateCreateInfo create_depth_stencil_state(
		const CreateInfo& info);

private:

	void create_pipeline(const CreateInfo& info);

private:
	Device* p_device;
	VkDevice m_vk_device;
	VkPipeline m_pipeline;
	VkPipelineLayout m_layout;
};

}}
