#include <stdexcept>
#include <SDL2/SDL_vulkan.h>

#include "window.hpp"

namespace tgal {
namespace vulkan {

Window::Window(const CreateInfo& info)
	: m_flags(info.flags)
{
	window = initialize_sdl(info);
}

Window::~Window()
{
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void Window::create_surface(const VkInstance& instance)
{
	if (!SDL_Vulkan_CreateSurface(window, instance, &surface))
		throw std::runtime_error("Failed to create surface.");
	m_instance = instance;
}

void Window::destroy_surface()
{
	vkDestroySurfaceKHR(m_instance, surface, nullptr);
}


SDL_Window* Window::initialize_sdl(const CreateInfo& info)
{
	// Print SDL version.
	print_sdl_info();

	// Initialize video.
	int res = SDL_Init(SDL_INIT_VIDEO);
	if (res != 0) {
		throw std::runtime_error(
			std::string("Failed to initialize SDL: ") + SDL_GetError()
		);
	}

	// Create window.
	SDL_Window* window = SDL_CreateWindow(
		info.title,
		info.x, info.y,
		info.w, info.h,
		SDL_WINDOW_VULKAN | m_flags
	);
	if (!window) {
		throw std::runtime_error(
			std::string("Couldn't create window: ") + SDL_GetError()
		);
	}

	// Return.
	return window;
}

void Window::print_sdl_info()
{
	// Compiled version.
	SDL_version compiled;
	SDL_VERSION(&compiled);
	printf("Compiled against SDL %d.%d.%d.\n",
		   compiled.major, compiled.minor, compiled.patch);

	// Linked version.
	SDL_version linked;
	SDL_GetVersion(&linked);
	printf("Linked against SDL %d.%d.%d.\n\n",
		   linked.major, linked.minor, linked.patch);
}

}}
