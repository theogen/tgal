#include "utils.hpp"

using namespace tgal;

namespace tgal {
namespace vulkan {
namespace utils {

VkFormat get_vk_format(Format format) noexcept
{
	switch (format) {
		/* R */
		case Format::Unknown:
			return VK_FORMAT_UNDEFINED;

		case Format::R8UN:
			return VK_FORMAT_R8_UNORM;
		case Format::R8SN:
			return VK_FORMAT_R8_SNORM;
		case Format::R8SI:
			return VK_FORMAT_R8_SINT;
		case Format::R8UI:
			return VK_FORMAT_R8_UINT;

		case Format::R16UN:
			return VK_FORMAT_R16_UNORM;
		case Format::R16SN:
			return VK_FORMAT_R16_SNORM;
		case Format::R16SI:
			return VK_FORMAT_R16_SINT;
		case Format::R16UI:
			return VK_FORMAT_R16_UINT;
		case Format::R16SF:
			return VK_FORMAT_R16_SFLOAT;

		case Format::R32SI:
			return VK_FORMAT_R32_SINT;
		case Format::R32UI:
			return VK_FORMAT_R32_UINT;
		case Format::R32SF:
			return VK_FORMAT_R32_SFLOAT;

		/* RG */

		case Format::RG8UN:
			return VK_FORMAT_R8G8_UNORM;
		case Format::RG8SN:
			return VK_FORMAT_R8G8_UNORM;
		case Format::RG8SI:
			return VK_FORMAT_R8G8_SINT;
		case Format::RG8UI:
			return VK_FORMAT_R8G8_UINT;

		case Format::RG16UN:
			return VK_FORMAT_R16G16_UNORM;
		case Format::RG16SN:
			return VK_FORMAT_R16G16_SNORM;
		case Format::RG16SI:
			return VK_FORMAT_R16G16_SINT;
		case Format::RG16UI:
			return VK_FORMAT_R16G16_UINT;
		case Format::RG16SF:
			return VK_FORMAT_R16G16_SFLOAT;

		case Format::RG32SI:
			return VK_FORMAT_R32G32_SINT;
		case Format::RG32UI:
			return VK_FORMAT_R32G32_UINT;
		case Format::RG32SF:
			return VK_FORMAT_R32G32_SFLOAT;

		/* RGB */

		case Format::RGB8UN:
			return VK_FORMAT_R8G8B8_UNORM;
		case Format::RGB8SN:
			return VK_FORMAT_R8G8B8_SNORM;
		case Format::RGB8SI:
			return VK_FORMAT_R8G8B8_SINT;
		case Format::RGB8UI:
			return VK_FORMAT_R8G8B8_UINT;

		case Format::RGB16UN:
			return VK_FORMAT_R16G16B16_UNORM;
		case Format::RGB16SN:
			return VK_FORMAT_R16G16B16_SNORM;
		case Format::RGB16SI:
			return VK_FORMAT_R16G16B16_SINT;
		case Format::RGB16UI:
			return VK_FORMAT_R16G16B16_UINT;
		case Format::RGB16SF:
			return VK_FORMAT_R16G16B16_SFLOAT;

		case Format::RGB32SI:
			return VK_FORMAT_R32G32B32_SINT;
		case Format::RGB32UI:
			return VK_FORMAT_R32G32B32_UINT;
		case Format::RGB32SF:
			return VK_FORMAT_R32G32B32_SFLOAT;

		/* RGBA */

		case Format::RGBA8UN:
			return VK_FORMAT_R8G8B8A8_UNORM;
		case Format::RGBA8SN:
			return VK_FORMAT_R8G8B8A8_SNORM;
		case Format::RGBA8SI:
			return VK_FORMAT_R8G8B8A8_SINT;
		case Format::RGBA8UI:
			return VK_FORMAT_R8G8B8A8_UINT;

		case Format::RGBA16UN:
			return VK_FORMAT_R16G16B16A16_UNORM;
		case Format::RGBA16SN:
			return VK_FORMAT_R16G16B16A16_SNORM;
		case Format::RGBA16SI:
			return VK_FORMAT_R16G16B16A16_SINT;
		case Format::RGBA16UI:
			return VK_FORMAT_R16G16B16A16_UINT;
		case Format::RGBA16SF:
			return VK_FORMAT_R16G16B16A16_SFLOAT;

		case Format::RGBA32SI:
			return VK_FORMAT_R32G32B32A32_SINT;
		case Format::RGBA32UI:
			return VK_FORMAT_R32G32B32A32_UINT;
		case Format::RGBA32SF:
			return VK_FORMAT_R32G32B32A32_SFLOAT;

		/* BGR */

		case Format::BGR8UN:
			return VK_FORMAT_B8G8R8_UNORM;
		case Format::BGR8SN:
			return VK_FORMAT_B8G8R8_SNORM;
		case Format::BGR8UI:
			return VK_FORMAT_B8G8R8_UINT;
		case Format::BGR8SI:
			return VK_FORMAT_B8G8R8_SINT;

		/* BGRA */

		case Format::BGRA8UN:
			return VK_FORMAT_B8G8R8A8_UNORM;
		case Format::BGRA8SN:
			return VK_FORMAT_B8G8R8A8_SNORM;
		case Format::BGRA8UI:
			return VK_FORMAT_B8G8R8A8_UINT;
		case Format::BGRA8SI:
			return VK_FORMAT_B8G8R8A8_SINT;

		/* Depth */

		case Format::D16:
			return VK_FORMAT_D16_UNORM;
		case Format::D24S8:
			return VK_FORMAT_D24_UNORM_S8_UINT;
		case Format::D32S8:
			return VK_FORMAT_D32_SFLOAT_S8_UINT;
		case Format::D32SF:
			return VK_FORMAT_D32_SFLOAT;
	}

	return VK_FORMAT_UNDEFINED;
}

VmaMemoryUsage get_vma_memory_usage(MemoryAccess access) noexcept
{
	switch (access) {
		case MemoryAccess::GpuOnly:
			return VMA_MEMORY_USAGE_GPU_ONLY;
		case MemoryAccess::CpuOnly:
			return VMA_MEMORY_USAGE_CPU_ONLY;
		case MemoryAccess::CpuToGpu:
			return VMA_MEMORY_USAGE_CPU_TO_GPU;
		case MemoryAccess::GpuToCpu:
			return VMA_MEMORY_USAGE_GPU_TO_CPU;
		case MemoryAccess::Unknown:
			return VMA_MEMORY_USAGE_UNKNOWN;
	}

	return VMA_MEMORY_USAGE_UNKNOWN;
}

}}}
