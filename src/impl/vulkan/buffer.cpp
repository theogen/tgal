#include <stdexcept>

#include "buffer.hpp"
#include "device.hpp"
#include "command_buffer.hpp"
#include "utils.hpp"

namespace tgal {
namespace vulkan {

Buffer::Buffer(Device* device, const CreateInfo& info) :
	SwapchainDependant(info.swapchain),
	m_info(info),
	p_device(device),
	m_vk_device(device->vk_object()),
	m_allocator(device->allocator())
{
	create(info);
}

void Buffer::create(const CreateInfo& info)
{
	m_buffers_count = 1;
	if (info.swapchain != nullptr)
		m_buffers_count = info.swapchain->images_count();

	m_allocations.resize(m_buffers_count);
	m_buffers.resize(m_buffers_count);

	uint32_t usage = get_vk_usage(info.usage);
	if (need_staging_buffer(info))
		usage |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;

	for (uint32_t i = 0; i < m_buffers_count; ++i) {
		create_buffer(
			p_device,
			info.size,
			static_cast<VkBufferUsageFlagBits>(usage),
			info.access,
			VK_SHARING_MODE_EXCLUSIVE,
			&m_buffers[i],
			&m_allocations[i]
		);
	}

	if (need_staging_buffer(info)) {
		create_staging_buffer_and_copy_from_it(info);
	}
}

void Buffer::destroy()
{
	for (uint32_t i = 0; i < m_buffers_count; ++i) {
		vmaDestroyBuffer(m_allocator, m_buffers[i], m_allocations[i]);
	}
}

void Buffer::recreate(Swapchain* swapchain)
{
	// Don't recreate if the swapchain has the same number of images.
	if (swapchain->images_count() == m_buffers_count)
		return;
	destroy();
	create(m_info);
}

Buffer::~Buffer()
{
	destroy();
}

void Buffer::write(
	void* data,
	uint32_t size,
	uint32_t offset)
{
	void* mdata;
	uint32_t index = m_info.swapchain->current_image_index();
	map(index, &mdata);
	memcpy((uint8_t*)mdata + offset, data, size);
	unmap(index);
}

void Buffer::map(void** memory)
{
	vmaMapMemory(m_allocator, m_allocations[0], memory);
}

void Buffer::map(uint32_t index, void** memory)
{
	vmaMapMemory(m_allocator, m_allocations[index], memory);
}

void Buffer::unmap(uint32_t index)
{
	vmaUnmapMemory(m_allocator, m_allocations[index]);
}


void Buffer::create_buffer(
	Device* device,
	uint32_t size,
	VkBufferUsageFlagBits usage,
	MemoryAccess access,
	VkSharingMode sharing_mode,
	VkBuffer* buffer,
	VmaAllocation* alloc)
{
	VkBufferCreateInfo buffer_info = {};
	buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	buffer_info.size = size;
	buffer_info.usage = usage;
	buffer_info.sharingMode = sharing_mode;
	
	VmaAllocationCreateInfo alloc_info = {};
	alloc_info.usage = utils::get_vma_memory_usage(access);

	vmaCreateBuffer(
		device->allocator(),
		&buffer_info,
		&alloc_info,
		buffer,
		alloc,
		nullptr
	);
}

void Buffer::create_staging_buffer(
	Device* device,
	uint32_t size,
	const void* data,
	VkBuffer* buffer,
	VmaAllocation* alloc)
{
	create_buffer(
		device,
		size,
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		MemoryAccess::CpuOnly,
		VK_SHARING_MODE_EXCLUSIVE,
		buffer,
		alloc
	);

	void* memory;
	vmaMapMemory(device->allocator(), *alloc, &memory);
	memcpy(memory, data, size);
	vmaUnmapMemory(device->allocator(), *alloc);
}


bool Buffer::need_staging_buffer(const CreateInfo& info) const noexcept
{
#if 0
	return ((static_cast<uint32_t>(info.usage) & static_cast<uint32_t>(BufferUsage::IndexBuffer)) ||
		    (static_cast<uint32_t>(info.usage) & static_cast<uint32_t>(BufferUsage::VertexBuffer))) &&
		   info.access != MemoryAccess::CpuOnly;
#endif
	return info.usage == BufferUsage::IndexBuffer || info.usage == BufferUsage::VertexBuffer;
}

void Buffer::create_staging_buffer_and_copy_from_it(const CreateInfo& info)
{
	VkBuffer staging_buffer;
	VmaAllocation staging_buffer_alloc;

	create_staging_buffer(
		p_device,
		info.size,
		info.data,
		&staging_buffer,
		&staging_buffer_alloc
	);

	for (uint32_t i = 0; i < m_buffers_count; ++i) {
		// Copying buffers.
		CommandBuffer::CreateInfo cmd_buffer_info;
		cmd_buffer_info.one_time_submit = true;
		CommandBuffer* single_time_cmd
			= static_cast<CommandBuffer*>(p_device->create_command_buffer(cmd_buffer_info));

		single_time_cmd->begin();

		VkBufferCopy copy_region = {};
		copy_region.srcOffset = 0;
		copy_region.dstOffset = 0;
		copy_region.size = info.size;
		vkCmdCopyBuffer(
			single_time_cmd->vk_object(),
			staging_buffer,
			m_buffers[i],
			1, &copy_region
		);

		single_time_cmd->end();

		p_device->submit_single_time_commands(single_time_cmd);

		delete single_time_cmd;
	}

	vmaDestroyBuffer(m_allocator, staging_buffer, staging_buffer_alloc);
}

VkBufferUsageFlagBits Buffer::get_vk_usage(BufferUsage usage) const noexcept
{
	switch (usage) {
		case BufferUsage::IndexBuffer:
			return VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
		case BufferUsage::VertexBuffer:
			return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
		case BufferUsage::UniformBuffer:
			return VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
	}

	return static_cast<VkBufferUsageFlagBits>(0);
}

}}
