#include <stdexcept>
#include <iostream>

#include "descriptor_set.hpp"
#include "descriptor_set_layout.hpp"
#include "device.hpp"
#include "buffer.hpp"
#include "image.hpp"

namespace tgal {
namespace vulkan {

DescriptorSet::DescriptorSet(
	Device* device,
	const CreateInfo& info
) :
	SwapchainDependant(info.swapchain),
	m_info(info),
	p_device(device),
	m_vk_device(device->vk_object()),
	m_layout(info.layout)
{
	create(info);
}

void DescriptorSet::create(const CreateInfo& info)
{
	m_count = 1;
	if (info.swapchain != nullptr)
		m_count = info.swapchain->images_count();
	create_descriptor_pool(info);
	create_descriptor_set(info);
}

void DescriptorSet::recreate(Swapchain* swapchain)
{
	// Don't recreate if the swapchain has the same number of images.
	if (swapchain->images_count() == m_count)
		return;
	destroy();
	create(m_info);
}

void DescriptorSet::destroy()
{
	vkDestroyDescriptorPool(m_vk_device, m_descriptor_pool, nullptr);
}

DescriptorSet::~DescriptorSet()
{
	destroy();
}


void DescriptorSet::create_descriptor_pool(const CreateInfo& info)
{
	uint32_t bindings_count = m_layout->bindings().size();

	std::vector<VkDescriptorPoolSize> pool_sizes(bindings_count);

	for (size_t i = 0; i < bindings_count; ++i) {
		pool_sizes[i].type = DescriptorSetLayout::get_vk_type(m_layout->bindings()[i].type);
		pool_sizes[i].descriptorCount = m_count * m_layout->bindings()[i].count;
	}

	VkDescriptorPoolCreateInfo pool_info = {};
	pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	pool_info.poolSizeCount = pool_sizes.size();
	pool_info.pPoolSizes = pool_sizes.data();
	pool_info.maxSets = m_count;

	int res = vkCreateDescriptorPool(
		m_vk_device, &pool_info,
		nullptr, &m_descriptor_pool
	);

	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to create descriptor pool!");
	}
}

void DescriptorSet::create_descriptor_set(
	const DescriptorSet::CreateInfo& info)
{
	auto layout = static_cast<DescriptorSetLayout*>(m_layout);
	std::vector<VkDescriptorSetLayout> layouts(m_count, layout->vk_object());

	VkDescriptorSetAllocateInfo alloc_info = {};
	alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	alloc_info.descriptorPool = m_descriptor_pool;
	alloc_info.descriptorSetCount = m_count;
	alloc_info.pSetLayouts = layouts.data();

	m_sets.resize(m_count);
	int res = vkAllocateDescriptorSets(m_vk_device, &alloc_info, m_sets.data());
	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to allocate descriptor sets!");
	}

	for (uint32_t i = 0; i < m_sets.size(); ++i)
		create_writes(info, m_sets[i], i);
}

void DescriptorSet::create_writes(
	const CreateInfo& info, const VkDescriptorSet& set, uint32_t buffer_index)
{
	std::vector<VkWriteDescriptorSet> descriptor_writes(info.writes.size());
	size_t writes_count = info.writes.size();
	std::vector<std::vector<VkDescriptorBufferInfo>*> buffer_infos;

	for (size_t i = 0; i < writes_count; ++i) {
		auto binding = m_layout->bindings()[i];
		size_t descriptor_count = binding.count;

		auto& write = info.writes[i];
		descriptor_writes[i] = {};
		descriptor_writes[i].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptor_writes[i].dstSet = set;
		descriptor_writes[i].dstBinding = i;
		descriptor_writes[i].dstArrayElement = 0;
		descriptor_writes[i].descriptorCount = descriptor_count;

		if (write.use_buffer()) {
			auto buffer = static_cast<Buffer*>(write.buffer_info().buffer);
			buffer_infos.push_back(new std::vector<VkDescriptorBufferInfo>(descriptor_count));

			VkDescriptorBufferInfo buffer_info = {};
			buffer_info.buffer = buffer->vk_object(buffer_index);

			for (size_t j = 0; j < descriptor_count; ++j) {
				size_t element_size = write.buffer_info().range;
				buffer_info.offset = write.buffer_info().offset;
				buffer_info.range = element_size;
				(*buffer_infos.back())[j] = buffer_info;
			}

			descriptor_writes[i].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptor_writes[i].pBufferInfo = buffer_infos.back()->data();
		}
		else if (write.use_image()) {
			auto image = static_cast<Image*>(write.image_info().image);

			VkDescriptorImageInfo image_info = {};
			image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			image_info.imageView = image->vk_image_view();
			image_info.sampler = image->sampler();

			descriptor_writes[i].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			descriptor_writes[i].pImageInfo = &image_info;
		}
		else {
			throw std::runtime_error("All descriptor writes are null!");
		}
	}

	vkUpdateDescriptorSets(
		m_vk_device,
		descriptor_writes.size(), descriptor_writes.data(),
		0, nullptr
	);

	for (auto info : buffer_infos)
		delete info;
}

}}
