#include "commands.hpp"
#include "framebuffer.hpp"
#include "descriptor_set.hpp"

#include <vulkan/vulkan.h>

namespace tgal {
namespace vulkan {

namespace Commands {

void BeginRenderPass::record()
{
	if (p_buffer->swapchain() != nullptr)
		m_info.extent = p_buffer->swapchain()->extent();

	for (uint32_t i = 0; i < p_buffer->buffers_count(); ++i) {
		VkRenderPassBeginInfo render_pass_info = {};
		render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		render_pass_info.renderPass
			= static_cast<const RenderPass*>(m_info.render_pass)->vk_object();
		render_pass_info.framebuffer
			= static_cast<const Framebuffer*>(m_info.framebuffer)->vk_object(i);
		render_pass_info.renderArea.offset = {0, 0};
		render_pass_info.renderArea.extent = { m_info.extent.w, m_info.extent.h };
		std::array<VkClearValue, 2> clear_values = {};
		clear_values[0].color = {
			m_info.clear_color.r,
			m_info.clear_color.b,
			m_info.clear_color.g,
			m_info.clear_color.a
		};
		clear_values[1].depthStencil = {1.0f, 0};
		render_pass_info.clearValueCount = clear_values.size();
		render_pass_info.pClearValues = clear_values.data();

		vkCmdBeginRenderPass(
			p_buffer->vk_command_buffers()[i],
			&render_pass_info,
			VK_SUBPASS_CONTENTS_INLINE
		);
	}
}

void EndRenderPass::record()
{
	for (auto& buffer : p_buffer->vk_command_buffers()) {
		vkCmdEndRenderPass(buffer);
	}
}

void BindPipeline::record()
{
	for (auto& buffer : p_buffer->vk_command_buffers()) {
		vkCmdBindPipeline(
			buffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			p_pipeline->vk_object()
		);
	}
	p_buffer->set_current_pipeline(p_pipeline);
}

void CopyBuffer::record()
{
	VkBufferCopy copy_region = {};
	copy_region.srcOffset = 0;
	copy_region.dstOffset = 0;
	copy_region.size = m_size;
	for (auto& buffer : p_buffer->vk_command_buffers()) {
		vkCmdCopyBuffer(
			buffer,
			p_src->vk_object(),
			p_dst->vk_object(),
			1, &copy_region
		);
	}
}

void BindIndexBuffer::record()
{
	VkIndexType vk_type = VK_INDEX_TYPE_UINT32;
	if (m_type == IndexType::UInt16)
		vk_type = VK_INDEX_TYPE_UINT16;
	for (auto& buffer : p_buffer->vk_command_buffers()) {
		vkCmdBindIndexBuffer(
			buffer,
			p_index_buffer->vk_object(),
			m_offset, vk_type
		);
	}
}

void BindVertexBuffer::record()
{
	size_t buffers_count = m_bindings_count - m_first_binding;
	std::vector<VkBuffer> vertex_buffers(buffers_count);
	std::vector<VkDeviceSize> offsets(buffers_count);
	for (size_t i = 0; i < buffers_count; ++i)
	{
		vertex_buffers[i] = p_vertex_buffers[i].vk_object();
		offsets[i] = 0;
	}

	for (auto& buffer : p_buffer->vk_command_buffers()) {
		vkCmdBindVertexBuffers(
			buffer,
			m_first_binding,
			m_bindings_count,
			vertex_buffers.data(),
			offsets.data()
		);
	}
}

void BindDescriptorSets::record()
{
	// Pipeline layout.
	VkPipelineLayout layout = p_buffer->current_pipeline()->pipeline_layout();

	// Vulkan sets vector.
	std::vector<VkDescriptorSet> vk_sets(m_count);
	for (uint32_t i = 0; i < p_buffer->buffers_count(); ++i) {
		// Get each set of current image.
		for (uint32_t j = 0; j < m_count; ++j) {
			vk_sets[j] = static_cast<DescriptorSet*>(&p_sets[j])->vk_object(i);
		}

		// Bind the sets.
		vkCmdBindDescriptorSets(
			p_buffer->vk_command_buffers()[i],
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			layout,
			m_first_set, m_count, vk_sets.data(),
			0, nullptr
		);
	}
}

void SetViewport::record()
{
	if (p_buffer->swapchain() != nullptr)
	{
		m_viewport.w = p_buffer->swapchain()->extent().w;
		m_viewport.h = p_buffer->swapchain()->extent().h;
	}

	VkViewport vk_viewport;
	vk_viewport.x = m_viewport.x;
	vk_viewport.y = m_viewport.y;
	vk_viewport.width = m_viewport.w;
	vk_viewport.height = m_viewport.h;
	vk_viewport.minDepth = 0.0f;
	vk_viewport.maxDepth = 1.0f;

	for (auto& buffer : p_buffer->vk_command_buffers()) {
		vkCmdSetViewport(
			buffer,
			0, 1,
			&vk_viewport
		);
	}
}

void SetScissor::record()
{
	if (p_buffer->swapchain() != nullptr)
	{
		m_scissor.extent  = p_buffer->swapchain()->extent();
	}

	VkRect2D vk_scissor;
	vk_scissor.offset.x = m_scissor.offset.x;
	vk_scissor.offset.y = m_scissor.offset.y;
	vk_scissor.extent.width = m_scissor.extent.w;
	vk_scissor.extent.height = m_scissor.extent.h;

	for (auto& buffer : p_buffer->vk_command_buffers()) {
		vkCmdSetScissor(
			buffer,
			0, 1,
			&vk_scissor
		);
	}
}

void Draw::record()
{
	for (auto& buffer : p_buffer->vk_command_buffers()) {
		vkCmdDraw(
			buffer,
			m_vertex_count,
			m_instance_count,
			m_first_vertex,
			m_first_instance
		);
	}
}

void DrawIndexed::record()
{
	for (auto& buffer : p_buffer->vk_command_buffers()) {
		vkCmdDrawIndexed(
			buffer,
			m_index_count,
			m_instance_count,
			m_first_index,
			m_vertex_offset,
			m_first_instance
		);
	}
}

}}} // namespace tgal::vulkan::Commands
