#include <iostream>
#include <algorithm>
#include <SDL2/SDL_vulkan.h>

#include "swapchain.hpp"
#include "swapchain_creator.hpp"
#include "device.hpp"

namespace tgal {
namespace vulkan {

SwapchainCreator::SwapchainCreator(
	Device* device
) :
	m_physical_device(device->physical_device()->vk_object()),
	p_device(device),
	m_device(device->vk_object()),
	m_queue_families(device->physical_device()->queue_families())
{}

Swapchain* SwapchainCreator::create_swapchain(Window* window)
{
	// Swap chain support details.
	SwapchainSupportDetails swapchain_support
		= SwapchainCreator::query_support(m_physical_device, window->surface);

	// Surface format.
	VkSurfaceFormatKHR surface_format
		= choose_surface_format(swapchain_support.formats);

	// Present mode.
	VkPresentModeKHR present_mode
		= choose_present_mode(swapchain_support.present_modes);

	// Extent.
	VkExtent2D extent = choose_extent(window, swapchain_support.capabilities);

	// Creating VkSwapchain.
	VkSwapchainKHR vk_swapchain = create_vk_swapchain(
		window->surface,
		swapchain_support,
		surface_format,
		present_mode,
		extent
	);

	// Creating the swapchain object.
	Swapchain* swapchain = new Swapchain(
		this,
		window,
		vk_swapchain,
		swapchain_support,
		surface_format,
		present_mode,
		extent
	);
	
	// Returning the swapchain object.
	return swapchain;
}

VkSwapchainKHR SwapchainCreator::create_vk_swapchain(
	VkSurfaceKHR surface,
	SwapchainSupportDetails swapchain_support,
	VkSurfaceFormatKHR surface_format,
	VkPresentModeKHR present_mode,
	VkExtent2D extent)
{
	// How many images we would like to have in the swap chain.
	//
	// It is recommended to request at least one more image than minimum
	// so we don't have to wait on the driver to complete internal operations
	// before we can aquire another image to render to.
	uint32_t image_count = swapchain_support.capabilities.minImageCount + 1;

	// Fallback to max image count if it is less than we requested.
	if (swapchain_support.capabilities.maxImageCount > 0 &&
		swapchain_support.capabilities.maxImageCount < image_count)
	{
		image_count = swapchain_support.capabilities.maxImageCount;
	}

	// Swapchain create info.
	VkSwapchainCreateInfoKHR create_info = {};
	create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	// Surface to draw to.
	create_info.surface = surface;
	// Images count we chose above.
	create_info.minImageCount = image_count;
	create_info.imageFormat = surface_format.format;
	create_info.imageColorSpace = surface_format.colorSpace;
	create_info.imageExtent = extent;
	// The imageArrayLayers specifies the amount of layers each image consists
	// of. This is always 1 unless you are developing a stereoscopic 3D
	// application.
	create_info.imageArrayLayers = 1;
	// The imageUsage bit field specifies what kind of operations we'll use the
	// images in the swap chain for.
	create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	// Queue families.
	uint32_t queue_family_indices[] = {
		static_cast<uint32_t>(m_queue_families.graphics),
		static_cast<uint32_t>(m_queue_families.present)
	};

	// Next, we need to specify how to handle swap chain images that will be
	// used across multiple queue families. That will be the case in our
	// application if the graphics queue family is different from the
	// presentation queue. We'll be drawing on the images in the swap chain
	// from the graphics queue and then submitting them on the presentation
	// queue.
	//
	// If the queue families differ, then we'll be using the concurrent mode.
	if (m_queue_families.graphics != m_queue_families.present) {
		create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		create_info.queueFamilyIndexCount = 2;
		create_info.pQueueFamilyIndices = queue_family_indices;
	}
	else {
		create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	}

	// We can specify that a certain transform should be applied to images in
	// the swap chain if it is supported (supportedTransforms in capabilities),
	// like a 90 degree clockwise rotation or horizontal flip. To specify that
	// you do not want any transformation, simply specify the current
	// transformation.
	create_info.preTransform = swapchain_support.capabilities.currentTransform;
	// The compositeAlpha field specifies if the alpha channel should be used
	// for blending with other windows in the window system. You'll almost
	// always want to simply ignore the alpha channel, hence
	// VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR.
	create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	create_info.presentMode = present_mode;
	// If the clipped member is set to VK_TRUE then that means that we don't
	// care about the color of pixels that are obscured, for example because
	// another window is in front of them. 
	create_info.clipped = VK_TRUE;
	// With Vulkan it's possible that your swap chain becomes invalid or
	// unoptimized while your application is running, for example because the
	// window was resized. In that case the swap chain actually needs to be
	// recreated from scratch and a reference to the old one must be specified
	// in this field.
	create_info.oldSwapchain = VK_NULL_HANDLE;

	// Creating swap chain.
	VkSwapchainKHR vk_swapchain;
	int res = vkCreateSwapchainKHR(
		m_device,
		&create_info,
		nullptr,
		&vk_swapchain
	);

	// Checking the result.
	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to create swap chain!");
	}

	return vk_swapchain;
}


VkSurfaceFormatKHR SwapchainCreator::choose_surface_format(
	const std::vector<VkSurfaceFormatKHR>& available_formats)
{
	// Wanted format.
	const VkFormat wanted_format = VK_FORMAT_B8G8R8A8_UNORM;
	// Wanted colorspace.
	const VkColorSpaceKHR wanted_colorspace
		= VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;

	// In case Vulkan lets us choose any format, we take the wanted one.
	if (available_formats.size() == 1 &&
		available_formats[0].format == VK_FORMAT_UNDEFINED)
	{
		return { wanted_format, wanted_colorspace };
	}

	// In case we get to choose from list of formats, try to choose the wanted
	// one.
	for (const auto& available_format : available_formats) {
		if (available_format.format == wanted_format &&
			available_format.colorSpace == wanted_colorspace)
		{
			return available_format;
		}
	}

	// Otherwise choose the first one.
	return available_formats[0];
}

VkPresentModeKHR SwapchainCreator::choose_present_mode(
	const std::vector<VkPresentModeKHR> available_present_modes)
{
	// FIFO (First In, First Out) as a fallback. Guaranteed to be available.
	VkPresentModeKHR best_mode = VK_PRESENT_MODE_FIFO_KHR;

	for (const auto& available_present_mode : available_present_modes) {
		// Mailbox is preferred. Return it immediately if available.
		if (available_present_mode == VK_PRESENT_MODE_MAILBOX_KHR)
			return available_present_mode;
		// Use immediate mode if mailbox isn't available.
		else if (available_present_mode == VK_PRESENT_MODE_IMMEDIATE_KHR)
			return best_mode = available_present_mode;
	}

	return best_mode;
}

VkExtent2D SwapchainCreator::choose_extent(
	Window* window,
	const VkSurfaceCapabilitiesKHR& capabilities)
{
	if (capabilities.currentExtent.width !=
		std::numeric_limits<uint32_t>::max())
	{
		return capabilities.currentExtent;
	}

	// Get the framebuffer size.
	int width, height;
	SDL_Vulkan_GetDrawableSize(window->window, &width, &height);

	// The extent object.
	VkExtent2D actual_extent = {
		static_cast<uint32_t>(width),
		static_cast<uint32_t>(height)
	};
	
	// Use either actual extent or max extent if it is less than the actual and
	// greater than minimal. Otherwise use minimal.
	actual_extent.width = std::max(
		capabilities.minImageExtent.width,
		std::min(capabilities.maxImageExtent.width, actual_extent.width));
	actual_extent.height = std::max(
		capabilities.minImageExtent.height,
		std::min(capabilities.maxImageExtent.height, actual_extent.height));

	return actual_extent;
}

SwapchainSupportDetails SwapchainCreator::query_support(
		const VkPhysicalDevice& device, const VkSurfaceKHR& surface)
{
	SwapchainSupportDetails details;

	// Device capabilities.
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
			device, surface, &details.capabilities);

	// Filling formats list.
	uint32_t format_count;
	vkGetPhysicalDeviceSurfaceFormatsKHR(
			device, surface, &format_count, nullptr);
	if (format_count != 0) {
		details.formats.resize(format_count);
		vkGetPhysicalDeviceSurfaceFormatsKHR(
			device, surface, &format_count, details.formats.data()
		);
	}

	// Filling present modes list.
	uint32_t present_mode_count;
	vkGetPhysicalDeviceSurfacePresentModesKHR(
			device, surface, &present_mode_count, nullptr);
	if (present_mode_count != 0) {
		details.present_modes.resize(present_mode_count);
		vkGetPhysicalDeviceSurfacePresentModesKHR(
			device, surface,
			&present_mode_count, details.present_modes.data()
		);
	}

	return details;
}


}}
