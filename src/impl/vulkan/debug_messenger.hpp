#pragma once

#include <vector>
#include <array>
#include <unordered_map>
#include <string>
#include <vulkan/vulkan.h>

#include "../../enum_flag_operators.hpp"

namespace tgal {
namespace vulkan {

//! Message severity.
enum class MessageSeverity {
	Verbose = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT,
	Info    = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT,
	Warning = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT,
	Error   = VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
}; DEFINE_ENUM_FLAG_OPERATORS(MessageSeverity)

//! Message type.
enum class MessageType {
	General     = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
	Validation  = VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT,
	Performance = VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT
}; DEFINE_ENUM_FLAG_OPERATORS(MessageType)

/*!
 * Wrapper around VkDebugUtilsMessengerEXT with some extra features such as
 * ignoring duplicate messages and replacing contents of messages.
 */
class DebugMessenger {
public:
	typedef std::vector<std::array<const char*, 2>> Replacements;

	struct CreateInfo {
		//! Severity of the messages.
		MessageSeverity severity;
		//! Type of messages.
		MessageType type;
		//! Don't print duplicate messages.
		bool no_duplicates;
		//! Mute the messenger.
		bool mute;
		//! Process all messages through regex replacements.
		//! If a replacement is evaluated to an empty string,
		//! message won't be printed.
		Replacements replacements;
	};

public:
	DebugMessenger(const VkInstance& instance, const CreateInfo& info);
	~DebugMessenger();

	/* Getters */

	bool mute() const { return m_mute; }
	bool no_duplicates() const { return m_no_duplicates; }
	Replacements replacements() const { return m_replacements; }

	/* Setters */

	void set_mute(bool state) { m_mute = state; }
	void set_no_duplicates(bool state) { m_no_duplicates = state; }
	void set_replacements(const Replacements& replacements)
	{ m_replacements = replacements; }

	//! The function that gets called on a message.
	void callback(
		MessageSeverity severity,
		MessageType type,
		const VkDebugUtilsMessengerCallbackDataEXT* callback_data);

private:
	//! Process a message through regex
	//! replacements specified in m_replacements.
	std::string replace(const char* message);

	int type_name_index(MessageType type);
	int severity_color_index(MessageSeverity severity);

private:
	//! Messages are prefixed with a type name.
	const char* m_type_names[3] = { "GEN", "VAL", "PRF" };
	//! Type names are colored by severity colors.
	const char* m_severity_colors[4] = {
		"\e[36m", "\e[32m", "\e[33m", "\e[31m"
	};

	//! Vulkan instance.
	VkInstance m_instance;
	//! Messenger.
	VkDebugUtilsMessengerEXT m_debug_messenger;

	//! No duplicates.
	bool m_no_duplicates;
	//! Mute.
	bool m_mute;
	//! Regex replacements array.
	std::vector<std::array<const char*, 2>> m_replacements;

	//! Copy of last message (to determine duplicates).
	std::string m_last_message;
	//! Duplicates count.
	size_t m_duplicates_count = 1;
};

}}
