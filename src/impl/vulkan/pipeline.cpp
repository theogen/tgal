#include <vector>
#include <vulkan/vulkan.h>
#include <iostream>

#include "pipeline.hpp"
#include "shader.hpp"
#include "render_pass.hpp"
#include "descriptor_set_layout.hpp"
#include "utils.hpp"

namespace tgal {
namespace vulkan {

Pipeline::Pipeline(Device* device, CreateInfo info)
	: p_device(device), m_vk_device(device->vk_object())
{
	create_pipeline(info);
}

Pipeline::~Pipeline()
{
	vkDestroyPipeline(m_vk_device, m_pipeline, nullptr);
	vkDestroyPipelineLayout(m_vk_device, m_layout, nullptr);
}


void Pipeline::create_pipeline(const CreateInfo& info)
{
	// Pipeline.
	VkGraphicsPipelineCreateInfo pipeline_info = {};
	pipeline_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;

	// Shader stages.
	auto stages = create_shader_stage_infos(info);
	pipeline_info.stageCount = stages.size();
	pipeline_info.pStages = stages.data();

	// Vertex input.
	// Bindings array.
	std::vector<VkVertexInputBindingDescription> bindings;
	bindings.reserve(info.vertex_input.bindings.size());
	// Attributes array.
	std::vector<VkVertexInputAttributeDescription> attributes;
	attributes.reserve(info.vertex_input.attributes.size());

	auto vertex_input_info
		= create_vertex_input_info(info, bindings, attributes);
	pipeline_info.pVertexInputState = &vertex_input_info;

	// Input assembly.
	auto input_assembly_info = create_input_assembly_info(info);
	pipeline_info.pInputAssemblyState = &input_assembly_info;

	// Viewport & scissors.
	VkViewport viewport = {};
	VkRect2D scissor = {};

	auto viewport_info = create_viewport_info(info, viewport, scissor);
	pipeline_info.pViewportState = &viewport_info;

	// Rasterization.
	auto rasterization_info = create_rasterization_info(info);
	pipeline_info.pRasterizationState = &rasterization_info;

	// Multisampling.
	auto multisample_info = create_multisample_info(info);
	pipeline_info.pMultisampleState = &multisample_info;

	// Color blending.
	VkPipelineColorBlendAttachmentState color_blend_attachment = {};
	auto color_blend_info
		= create_color_blend_info(info, color_blend_attachment);
	pipeline_info.pColorBlendState = &color_blend_info;

	// Pipeline layout.
	m_layout = create_pipeline_layout(info);
	pipeline_info.layout = m_layout;

	// Render pass
	auto render_pass = static_cast<RenderPass*>(info.render_pass)->vk_object();
	pipeline_info.renderPass = render_pass;
	pipeline_info.subpass = 0;

	// Depth stencil.
	auto depth_stencil = create_depth_stencil_state(info);
	pipeline_info.pDepthStencilState = &depth_stencil;

	// Dynamic state.
	VkDynamicState dynamic_states[] = {
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_SCISSOR
	};

	VkPipelineDynamicStateCreateInfo dynamic_state = {};
	dynamic_state.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamic_state.dynamicStateCount = 2;
	dynamic_state.pDynamicStates = dynamic_states;
	pipeline_info.pDynamicState = &dynamic_state;

	// Base pipeline.
	pipeline_info.basePipelineHandle = VK_NULL_HANDLE;
	pipeline_info.basePipelineIndex = -1;

	// Creating the pipeline.
	int res = vkCreateGraphicsPipelines(
		m_vk_device,
		VK_NULL_HANDLE,
		1, &pipeline_info,    // Create infos array.
		nullptr, &m_pipeline
	);

	// Checking result.
	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to create graphics pipeline!");
	}
}


std::vector<VkPipelineShaderStageCreateInfo>
	Pipeline::create_shader_stage_infos(const CreateInfo& info)
{
	// Stages array.
	std::vector<VkPipelineShaderStageCreateInfo> stages;
	stages.reserve(info.stages.size());

	// Create stages.
	for (const auto& stage : info.stages) {
		// Shader stage info.
		VkPipelineShaderStageCreateInfo stage_info = {};
		stage_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		// Shader stage.
		stage_info.stage = ShaderModule::vk_shader_stage(stage.stage);
		// Shader module.
		stage_info.module = static_cast<ShaderModule*>(stage.module)->vk_object();
		// Entry point.
		stage_info.pName = stage.entry.c_str();
		// Append the stage.
		stages.push_back(stage_info);
	}

	return stages;
}

VkPipelineVertexInputStateCreateInfo Pipeline::create_vertex_input_info(
	const CreateInfo& info,
	std::vector<VkVertexInputBindingDescription>& bindings,
	std::vector<VkVertexInputAttributeDescription>& attributes
)
{
	// Create binding descriptions.
	for (const VertexInput::BindingDesc& binding : info.vertex_input.bindings) {
		// Binding description.
		VkVertexInputBindingDescription binding_desc = {};
		// Binding.
		binding_desc.binding = binding.binding;
		// Stride.
		binding_desc.stride = binding.stride;
		// Input rate.
		// - Per-vertex.
		if (binding.rate == VertexInput::Rate::Vertex)
			binding_desc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		// - Per-instance.
		if (binding.rate == VertexInput::Rate::Instance)
			binding_desc.inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;

		// Append.
		bindings.push_back(binding_desc);
	}

	// Create attribute descriptions.
	for (const VertexInput::AttributeDesc& attr : info.vertex_input.attributes) {
		// Attribute description.
		VkVertexInputAttributeDescription attr_desc = {};
		// Binding.
		attr_desc.binding = attr.binding;
		// Location.
        attr_desc.location = attr.location;
		// Format.
        attr_desc.format = utils::get_vk_format(attr.format);
		// Offset.
        attr_desc.offset = attr.offset;

		// Append.
		attributes.push_back(attr_desc);
	}

	// Debug info.
#if 0
	std::cerr << "Bindings:\n";

	for (const auto& binding : bindings)
	{
		std::cerr << "  Binding: " << binding.binding << '\n';
		std::cerr << "  Stride: " << binding.stride << '\n';
		std::cerr << "---\n";
	}

	std::cerr << "Attributes:\n";

	for (const auto& attr : attributes)
	{
		std::cerr << "  Binding: " << attr.binding << '\n';
		std::cerr << "  Location: " << attr.location << '\n';
		std::cerr << "  Offset: " << attr.offset << '\n';
		std::cerr << "---\n";
	}
#endif

	// Vertex input info.
	VkPipelineVertexInputStateCreateInfo vertex_input_info = {};
	vertex_input_info.sType
		= VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertex_input_info.vertexBindingDescriptionCount = bindings.size();
	vertex_input_info.pVertexBindingDescriptions = bindings.data();
	vertex_input_info.vertexAttributeDescriptionCount = attributes.size();
	vertex_input_info.pVertexAttributeDescriptions = attributes.data();

	return vertex_input_info;
}

VkPipelineInputAssemblyStateCreateInfo
	Pipeline::create_input_assembly_info(const CreateInfo& info)
{
	VkPipelineInputAssemblyStateCreateInfo input_assembly = {};
	input_assembly.sType
		= VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	// Topology.
	input_assembly.topology = get_vk_topology(info.input_assembly.topology);
	// Primitive restart.
	input_assembly.primitiveRestartEnable = info.input_assembly.primitive_restart;

	return input_assembly;
}

VkPipelineViewportStateCreateInfo Pipeline::create_viewport_info(
	const CreateInfo& info, VkViewport& viewport, VkRect2D& scissor)
{
	// Viewport.
	viewport.x = info.viewport.x;
	viewport.y = info.viewport.y;
	viewport.width = info.viewport.w;
	viewport.height = info.viewport.h;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	// Scissor.
	scissor.offset.x = info.scissor.offset.x;
	scissor.offset.y = info.scissor.offset.y;
	scissor.extent.width = info.scissor.extent.w;
	scissor.extent.height = info.scissor.extent.h;

	// Viewport info.
	VkPipelineViewportStateCreateInfo viewport_info = {};
	viewport_info.sType
		= VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewport_info.viewportCount = 1;
	viewport_info.pViewports = &viewport;
	viewport_info.scissorCount = 1;
	viewport_info.pScissors = &scissor;

	return viewport_info;
}

VkPipelineRasterizationStateCreateInfo
	Pipeline::create_rasterization_info(const CreateInfo& info)
{
	// Rasterizer.
	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType
		= VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	// Polygon mode.
	switch (info.rasterization.polygon_mode) {
		case Rasterization::PolygonMode::Fill:
			rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
			break;
		case Rasterization::PolygonMode::Line:
			rasterizer.polygonMode = VK_POLYGON_MODE_LINE;
			break;
		case Rasterization::PolygonMode::Point:
			rasterizer.polygonMode = VK_POLYGON_MODE_POINT;
			break;
	}
	// Line width.
	rasterizer.lineWidth = info.rasterization.line_width;
	// Cull mode.
	switch (info.rasterization.cull_mode) {
		case Rasterization::CullMode::None:
			rasterizer.cullMode = VK_CULL_MODE_NONE;
			break;
		case Rasterization::CullMode::Back:
			rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
			break;
		case Rasterization::CullMode::Front:
			rasterizer.cullMode = VK_CULL_MODE_FRONT_BIT;
			break;
		case Rasterization::CullMode::BackAndFront:
			rasterizer.cullMode = VK_CULL_MODE_FRONT_AND_BACK;
			break;
	}
	// Front face.
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	// Depth bias.
	rasterizer.depthBiasEnable = VK_FALSE;
	rasterizer.depthBiasConstantFactor = 0.0f;
	rasterizer.depthBiasClamp = 0.0f;
	rasterizer.depthBiasSlopeFactor = 0.0f;

	return rasterizer;
}

VkPipelineMultisampleStateCreateInfo
	Pipeline::create_multisample_info(const CreateInfo& info)
{
	// Multisampling.
	// Disabled.
	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType
		= VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampling.minSampleShading = 1.0f;
	multisampling.pSampleMask = nullptr;
	multisampling.alphaToCoverageEnable = VK_FALSE;
	multisampling.alphaToOneEnable = VK_FALSE;

	return multisampling;
}

VkPipelineColorBlendStateCreateInfo Pipeline::create_color_blend_info(
	const CreateInfo& info,
	VkPipelineColorBlendAttachmentState& color_blend_attachment
)
{
	// Color write mask.
	color_blend_attachment.colorWriteMask =
		VK_COLOR_COMPONENT_R_BIT |
		VK_COLOR_COMPONENT_G_BIT |
		VK_COLOR_COMPONENT_B_BIT |
		VK_COLOR_COMPONENT_A_BIT;
	// Disable blending.
	color_blend_attachment.blendEnable = VK_FALSE;

	// Color blend.
	VkPipelineColorBlendStateCreateInfo color_blending = {};
	color_blending.sType
		= VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	color_blending.logicOpEnable = VK_FALSE;
	color_blending.logicOp = VK_LOGIC_OP_COPY; // Optional
	color_blending.attachmentCount = 1;
	color_blending.pAttachments = &color_blend_attachment;
	color_blending.blendConstants[0] = 0.0f; // Optional
	color_blending.blendConstants[1] = 0.0f; // Optional
	color_blending.blendConstants[2] = 0.0f; // Optional
	color_blending.blendConstants[3] = 0.0f; // Optional

	return color_blending;
}

VkPipelineLayout Pipeline::create_pipeline_layout(const CreateInfo& info)
{
	std::vector<VkDescriptorSetLayout> desc_set_layouts;
	desc_set_layouts.reserve(info.layout.layouts.size());
	for (auto layout : info.layout.layouts)
		desc_set_layouts.push_back(static_cast<DescriptorSetLayout*>(layout)->vk_object());

	// Pipeline layout.
	VkPipelineLayoutCreateInfo pipeline_layout_info = {};
	pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipeline_layout_info.setLayoutCount = desc_set_layouts.size();
	pipeline_layout_info.pSetLayouts = desc_set_layouts.data();
	pipeline_layout_info.pushConstantRangeCount = 0;
	pipeline_layout_info.pPushConstantRanges = nullptr;

	// Creating the layout.
	VkPipelineLayout layout;
	int res = vkCreatePipelineLayout(
		m_vk_device,
		&pipeline_layout_info,
		nullptr, &layout
	);

	// Checking.
	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to create pipeline layout!");
	}

	return layout;
}

VkPipelineDepthStencilStateCreateInfo Pipeline::create_depth_stencil_state(
	const CreateInfo& info)
{
	VkPipelineDepthStencilStateCreateInfo depth_stencil = {};
	depth_stencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depth_stencil.depthTestEnable = VK_TRUE;
	depth_stencil.depthWriteEnable = VK_TRUE;
	depth_stencil.depthCompareOp = VK_COMPARE_OP_LESS;
	depth_stencil.depthBoundsTestEnable = VK_FALSE;
	depth_stencil.minDepthBounds = 0.0f; // Optional
	depth_stencil.maxDepthBounds = 1.0f; // Optional
	depth_stencil.stencilTestEnable = VK_FALSE;
	depth_stencil.front = {}; // Optional
	depth_stencil.back = {}; // Optional

	return depth_stencil;
}


VkPrimitiveTopology Pipeline::get_vk_topology(PrimitiveTopology topology)
{
	// Convert API-agnostic enum to VkPrimitiveTopology.
	switch (topology) {
		case PrimitiveTopology::PointList:
			return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
		case PrimitiveTopology::LineList:
			return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
		case PrimitiveTopology::LineStrip:
			return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
		case PrimitiveTopology::TriangleList:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		case PrimitiveTopology::TriangleStrip:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
		case PrimitiveTopology::TriangleFan:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
		case PrimitiveTopology::LineListAdj:
			return VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
		case PrimitiveTopology::LineStripAdj:
			return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
		case PrimitiveTopology::TriangleListAdj:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
		case PrimitiveTopology::TriangleStripAdj:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
		case PrimitiveTopology::PatchList:
			return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
	}

	return VK_PRIMITIVE_TOPOLOGY_MAX_ENUM;
}

}}
