#include <iostream>
#include "render_pass.hpp"
#include "utils.hpp"

namespace tgal {
namespace vulkan {

RenderPass::RenderPass(Device* device, const CreateInfo& info) :
	SwapchainDependant(nullptr),
	m_info(info),
	p_device(device)
{
	for (const tgal::Image* image : info.attachments)
	{
		auto impl = dynamic_cast<const ImageImpl*>(image);
		if (impl->depends_on_swapchain()) {
			SwapchainDependant::set_swapchain(impl->swapchain());
			break;
		}
	}

	create(m_info);
}

RenderPass::~RenderPass()
{
	destroy();
}

void RenderPass::destroy()
{
	vkDestroyRenderPass(p_device->vk_object(), m_render_pass, nullptr);
	m_attachments.clear();
}

void RenderPass::recreate(Swapchain* swapchain)
{
	destroy();
	create(m_info);
}

void RenderPass::create(const CreateInfo& info)
{
	// Create attachments.
	std::vector<VkAttachmentDescription> attachments;
	std::vector<VkAttachmentReference> color_attachments_ref;
	bool has_depth_attachment;
	VkAttachmentReference depth_attachment_ref = {};

	for (size_t i = 0; i < info.attachments.size(); ++i) {
		auto image = dynamic_cast<const ImageImpl*>(info.attachments[i]);
		m_attachments.push_back(image);

		bool depth_stencil
			= image->usage() == ImageUsage::DepthStencilAttachment;

		auto attachment_desc = create_attachment_desc(
			image->vk_format(), depth_stencil);
		attachments.push_back(attachment_desc);

		if (depth_stencil)
		{
			depth_attachment_ref.attachment = i;
			depth_attachment_ref.layout =
				VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			has_depth_attachment = true;
		}
		else
		{
			VkAttachmentReference color_attachment_ref = {};
			color_attachment_ref.attachment = i;
			color_attachment_ref.layout =
				VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			color_attachments_ref.push_back(color_attachment_ref);
		}
	}

	// Subpass.
	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = color_attachments_ref.size();
	subpass.pColorAttachments = color_attachments_ref.data();
	if (has_depth_attachment)
		subpass.pDepthStencilAttachment = &depth_attachment_ref;

	// Dependency.
	VkSubpassDependency dependency = {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask =
		VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
		VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	// Render pass.
	VkRenderPassCreateInfo render_pass_info = {};
	render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	render_pass_info.attachmentCount = attachments.size();
	render_pass_info.pAttachments = attachments.data();
	render_pass_info.subpassCount = 1;
	render_pass_info.pSubpasses = &subpass;
	render_pass_info.dependencyCount = 1;
	render_pass_info.pDependencies = &dependency;

	// Creating the render pass.
	int res = vkCreateRenderPass(
		p_device->vk_object(),
		&render_pass_info,
		nullptr,
		&m_render_pass
	);

	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to create render pass!");
	}
}

VkAttachmentDescription RenderPass::create_attachment_desc(
	VkFormat format, bool depth_stencil)
{
		VkAttachmentDescription attachment_desc = {};
		attachment_desc.format = format;
		attachment_desc.samples = VK_SAMPLE_COUNT_1_BIT;
		attachment_desc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;

		if (depth_stencil)
			attachment_desc.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		else
			attachment_desc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

		attachment_desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		attachment_desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		attachment_desc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

		if (depth_stencil)
			attachment_desc.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		else
			attachment_desc.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		return attachment_desc;
}

}}
