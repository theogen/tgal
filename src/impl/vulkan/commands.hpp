#pragma once

#include "command_buffer.hpp"
#include "render_pass.hpp"
#include "pipeline.hpp"
#include "buffer.hpp"

namespace tgal {
namespace vulkan {

namespace Commands {

struct BeginRenderPass : public CommandBuffer::Command {
	BeginRenderPass(
		CommandBuffer* buffer,
		const RenderPass::BeginInfo& info
	) : p_buffer(buffer), m_info(info) {}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
	RenderPass::BeginInfo m_info;
};


struct EndRenderPass : public CommandBuffer::Command {
	EndRenderPass(CommandBuffer* buffer) : p_buffer(buffer) {}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
};


struct BindPipeline : public CommandBuffer::Command {
	BindPipeline(
		CommandBuffer* buffer,
		Pipeline* pipeline
	) : p_buffer(buffer), p_pipeline(pipeline) {}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
	Pipeline* p_pipeline;
};


struct CopyBuffer : public CommandBuffer::Command {
	CopyBuffer(
		CommandBuffer* buffer,
		Buffer* src,
		Buffer* dst,
		uint32_t size
	) : p_buffer(buffer), p_src(src), p_dst(dst), m_size(size) {}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
	Buffer *p_src, *p_dst;
	uint32_t m_size;
};


struct BindIndexBuffer : public CommandBuffer::Command {
	BindIndexBuffer(
		CommandBuffer* buffer,
		Buffer* index_buffer,
		IndexType type,
		uint32_t offset
	) :
		p_buffer(buffer),
		p_index_buffer(index_buffer),
		m_type(type),
		m_offset(offset) {}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
	Buffer *p_index_buffer;
	IndexType m_type;
	uint32_t m_offset;
};


struct BindVertexBuffer : public CommandBuffer::Command {
	BindVertexBuffer(
		CommandBuffer* buffer,
		uint32_t first_binding,
		uint32_t bindings_count,
		Buffer* vertex_buffers
	) :
		p_buffer(buffer),
		m_first_binding(first_binding),
		m_bindings_count(bindings_count),
		p_vertex_buffers(vertex_buffers)
	{}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
	uint32_t m_first_binding;
	uint32_t m_bindings_count;
	Buffer *p_vertex_buffers;
};


struct BindDescriptorSets : public CommandBuffer::Command {
	BindDescriptorSets(
		CommandBuffer* buffer,
		uint32_t first_set,
		size_t count,
		tgal::DescriptorSet* sets
	) :
		p_buffer(buffer),
		m_first_set(first_set),
		m_count(count),
		p_sets(sets)
	{}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
	uint32_t m_first_set;
	size_t m_count;
	tgal::DescriptorSet* p_sets;
};


struct SetViewport : public CommandBuffer::Command {
	SetViewport(
		CommandBuffer* buffer,
		Viewport viewport
	) :
		p_buffer(buffer),
		m_viewport(viewport)
	{}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
	Viewport m_viewport;
};


struct SetScissor : public CommandBuffer::Command {
	SetScissor(
		CommandBuffer* buffer,
		Rect2D scissor
	) :
		p_buffer(buffer),
		m_scissor(scissor)
	{}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
	Rect2D m_scissor;
};


struct Draw : public CommandBuffer::Command {
	Draw(
		CommandBuffer* buffer,
		uint32_t vertex_count,
		uint32_t instance_count,
		uint32_t first_vertex,
		uint32_t first_instance
	) :
		p_buffer(buffer),
		m_vertex_count(vertex_count),
		m_instance_count(instance_count),
		m_first_vertex(first_vertex),
		m_first_instance(first_instance)
	{}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
	uint32_t m_vertex_count;
	uint32_t m_instance_count;
	uint32_t m_first_vertex;
	uint32_t m_first_instance;
};


struct DrawIndexed : public CommandBuffer::Command {
	DrawIndexed(
		CommandBuffer* buffer,
		uint32_t index_count,
		uint32_t instance_count,
		uint32_t first_index,
		uint32_t vertex_offset,
		uint32_t first_instance
	) :
		p_buffer(buffer),
		m_index_count(index_count),
		m_instance_count(instance_count),
		m_first_index(first_index),
		m_vertex_offset(vertex_offset),
		m_first_instance(first_instance)
	{}

	virtual void record() override;

private:
	CommandBuffer* p_buffer;
	uint32_t m_index_count;
	uint32_t m_instance_count;
	uint32_t m_first_index;
	uint32_t m_vertex_offset;
	uint32_t m_first_instance;
};

}

}}
