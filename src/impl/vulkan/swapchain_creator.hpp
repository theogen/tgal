#pragma once

#include <vector>
#include <vulkan/vulkan.h>
#include <SDL2/SDL.h>

#include "physical_device.hpp"
#include "window.hpp"
#include "swapchain.hpp"

namespace tgal {
namespace vulkan {

class Device;

class SwapchainCreator {
public:
	SwapchainCreator(
		Device* device
	);

	/* Creation */

	Swapchain* create_swapchain(Window* window);

	VkSwapchainKHR create_vk_swapchain(
		VkSurfaceKHR surface,
		SwapchainSupportDetails swapchain_support,
		VkSurfaceFormatKHR surface_format,
		VkPresentModeKHR present_mode,
		VkExtent2D extent
	);

	/* Querying */

	VkSurfaceFormatKHR choose_surface_format(
			const std::vector<VkSurfaceFormatKHR>& available_formats);

	VkPresentModeKHR choose_present_mode(
			const std::vector<VkPresentModeKHR> available_present_modes);

	VkExtent2D choose_extent(
		Window* window,
		const VkSurfaceCapabilitiesKHR& capabilities
	);

	static SwapchainSupportDetails query_support(
			const VkPhysicalDevice& device, const VkSurfaceKHR& surface);

	/* Getters */

	Device* device() const { return p_device; }

private:
	VkPhysicalDevice m_physical_device;
	Device* p_device;
	VkDevice m_device;
	QueueFamilyIndices m_queue_families;
};

}}
