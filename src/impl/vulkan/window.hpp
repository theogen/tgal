#pragma once

#include <SDL2/SDL.h>
#include <vulkan/vulkan.h>

#include "interface/window.hpp"

namespace tgal {
namespace vulkan {

class Window : public tgal::Window {
public:
	SDL_Window* window;
	VkSurfaceKHR surface;

	Window(const CreateInfo& info);
	~Window();

	virtual SDL_Window* sdl_window() const noexcept override
	{ return window; }

	void create_surface(const VkInstance& instance);

	void destroy_surface();

private:
	void print_sdl_info();
	SDL_Window* initialize_sdl(const CreateInfo& info);

private:
	VkInstance m_instance;
	uint32_t m_flags;
};

}}
