#pragma once

#include <string>
#include <vulkan/vulkan.h>

#include "interface/physical_device.hpp"

namespace tgal {
namespace vulkan {

class PhysicalDeviceSelector;

struct QueueFamilyIndices {
	int graphics = -1;
	int present = -1;

	bool is_complete()
	{
		return graphics >= 0 && present >= 0;
	}
};

class PhysicalDevice : public tgal::PhysicalDevice {
public:
	PhysicalDevice(
		PhysicalDeviceSelector* selector,
		const char* name,
		const VkPhysicalDevice& device
	);

	virtual tgal::Device* create_device() override;

	virtual const char* name() const override { return m_name.c_str(); }

	VkPhysicalDevice vk_object() const { return m_physical_device; }
	QueueFamilyIndices queue_families() const { return m_queue_families; }

private:
	PhysicalDeviceSelector* p_selector;
	std::string m_name;
	VkPhysicalDevice m_physical_device;
	QueueFamilyIndices m_queue_families;
};

}}
