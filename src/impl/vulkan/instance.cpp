#include <iostream>
#include <stdexcept>
#include <string>

#include <SDL2/SDL_vulkan.h>

#include "instance.hpp"
#include "physical_device_selector.hpp"
#include "swapchain_creator.hpp"

#define MAKE_VERSION(VERSION) \
	VK_MAKE_VERSION(VERSION.major, VERSION.minor, VERSION.patch)

namespace tgal {
namespace vulkan {

Instance::Instance(const CreateInfo& info)
{
	// Creating window.
	p_window = static_cast<Window*>(info.window);
	// Creating instance.
	create_instance(info);
	// Creating debug messenger.
	create_debug_messenger();
	// Creating surface.
	p_window->create_surface(m_instance);

	// Physical device selector.
	m_physical_device_selector = new PhysicalDeviceSelector(this);
}

Instance::~Instance()
{
	delete p_debug_messenger;
	delete m_physical_device_selector;
	p_window->destroy_surface();
	vkDestroyInstance(m_instance, nullptr);
}

tgal::PhysicalDevice* Instance::pick_physical_device()
{
	return m_physical_device_selector->pick_device();
}


bool Instance::create_instance(const CreateInfo& info)
{
	// Check if validation layers are requested but not available.
	if (validation_layers_enabled() && !check_validation_layer_support())
		throw std::runtime_error("Validation layers are not available!");

	// Application info.
	VkApplicationInfo app_info = {};
	app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	app_info.pApplicationName = info.application_name;
	app_info.applicationVersion = MAKE_VERSION(info.application_version);
	app_info.pEngineName = info.engine_name;
	app_info.engineVersion = MAKE_VERSION(info.engine_version);
	app_info.apiVersion = VK_API_VERSION_1_0;

	// Create info.
	VkInstanceCreateInfo create_info = {};
	create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	create_info.pApplicationInfo = &app_info;

	// Instance extensions.
	std::vector<const char*> extensions = get_required_extensions(p_window->window);
	create_info.enabledExtensionCount = extensions.size();
	create_info.ppEnabledExtensionNames = extensions.data();

	if (!check_extensions_support(extensions)) {
		throw std::runtime_error(
			"Not all mandatory Vulkan extensions supported!"
		);
	}

		// Validation layers.
	if (validation_layers_enabled()) {
		create_info.enabledLayerCount = m_validation_layers.size();
		create_info.ppEnabledLayerNames = m_validation_layers.data();
	}

	if (vkCreateInstance(&create_info, nullptr, &m_instance) != VK_SUCCESS)
		throw std::runtime_error("Failed to create Vulkan instance!");

	return true;
}

std::vector<const char*> Instance::get_required_extensions(SDL_Window* window)
{
	// SDL extensions.
	uint32_t extension_count = 0;
	// Getting extension count.
	SDL_Vulkan_GetInstanceExtensions(
		window,
		&extension_count,
		nullptr
	);
	// Extensions array.
	std::vector<const char*> extensions(extension_count);
	// Adding the extensions.
	SDL_Vulkan_GetInstanceExtensions(
		window,
		&extension_count,
		extensions.data()
	);

	// Validation layers.
	if (validation_layers_enabled())
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

	return extensions;
}

bool Instance::check_extensions_support(const std::vector<const char*>& extensions)
{
	// Querying available extensions.
	uint32_t extension_count = 0;
	// Getting extension count.
	vkEnumerateInstanceExtensionProperties(
		nullptr, &extension_count, nullptr
	);
	// Extensions array.
	std::vector<VkExtensionProperties> supported_extensions(extension_count);
	// Filling the array.
	vkEnumerateInstanceExtensionProperties(
		nullptr, &extension_count, supported_extensions.data()
	);

	// Iterating through requested extensions.
	for (const auto& extension : extensions) {
		bool supported = false;

		// Iterating through supported extensions.
		for (const auto& supported_ext : supported_extensions) {
			if (0 == strcmp(extension, supported_ext.extensionName)) {
				supported = true;
				break;
			}
		}

		// Terminate if a requested extension isn't supported.
		if (!supported)
			return false;
	}

	return true;
}

bool Instance::check_validation_layer_support()
{
		// Available layers array.
		uint32_t layer_count;
		vkEnumerateInstanceLayerProperties(&layer_count, nullptr);
		std::vector<VkLayerProperties> available_layers(layer_count);
		vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());

#if 0
		std::cerr << "Available validation layers:\n";
		// Print all available validation layers.
		for (const auto& layer_properties : available_layers) {
			std::cerr << "- " << layer_properties.layerName << '\n';
		}
		std::cerr << '\n';
#endif

		// Iterating through requested validation layers.
		for (const char* layer_name : m_validation_layers) {
			bool supported = false;

			// Iterating through available iteration layers.
			for (const auto& layer_properties : available_layers) {
				if (strcmp(layer_name, layer_properties.layerName) == 0) {
					supported = true;
					break;
				}
			}

			// Terminate if a requested layer isn't supported.
			if (!supported)
				return false;
		}

		return true;
}


void Instance::create_debug_messenger()
{
	// Return if validation layers are disabled.
	if (!validation_layers_enabled())
		return;

	// Debug messenger.
	DebugMessenger::CreateInfo info = {};
	// Severity.
	info.severity =
		MessageSeverity::Verbose |
		MessageSeverity::Info |
		MessageSeverity::Warning |
		MessageSeverity::Error;
	// Type.
	info.type =
		MessageType::General | 
		MessageType::Validation | 
		MessageType::Performance;
	// Hide duplicated messages.
	info.no_duplicates = true;
	// Don't mute the messenger.
	info.mute = false;
	// Hide unwanted messages.
	info.replacements = {
		{ "Device Extension: .*$", "" }
		//{ " The Vulkan spec states: .*$", "" }
	};

	p_debug_messenger = new DebugMessenger(m_instance, info);
}

}}
