#pragma once

#include <vulkan/vulkan.h>
#include "vk_mem_alloc.h"

#include "interface/image.hpp"
#include "swapchain_dependant.hpp"

namespace tgal {
namespace vulkan {

class Device;

class ImageImpl : public tgal::Image, public SwapchainDependant {
public:
	ImageImpl(Swapchain* swapchain);

	virtual VkFormat vk_format() const = 0;
	virtual VkImageView vk_image_view(size_t i = 0) const = 0;
};

class Image : public ImageImpl {
public:
	Image(Device* device, const CreateInfo& info);
	~Image();

	virtual void recreate(Swapchain* swapchain) override;

	virtual uint32_t width() const noexcept override
	{ return m_info.width; }
	virtual uint32_t height() const noexcept override
	{ return m_info.height; }
	virtual ImageUsage usage() const override { return m_usage; }

	void create_image(
		uint32_t width, uint32_t height,
		VkFormat format,
		VkImageUsageFlagBits usage,
		MemoryAccess access,
		uint32_t mip_levels,
		VkImage* image, VmaAllocation* alloc
	);

	virtual VkFormat vk_format() const override { return m_format; }
	virtual VkImageView vk_image_view(size_t i = 0) const override { return m_view; }
	VkSampler sampler() const { return m_sampler; }

private:
	void create();
	void create_texture_image(
		const void* data,
		uint32_t width,
		uint32_t height,
		uint32_t mip_levels
	);

	void destroy();

	void transition_image_layout(
		const VkImage& image, VkFormat format,
		VkImageLayout old_layout, VkImageLayout new_layout,
		uint32_t mip_levels
	);

	void copy_buffer_to_image(
		VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);

	void generate_mipmaps(
		VkImage& image, VkFormat format,
		uint32_t w, uint32_t h, uint32_t mip_levels);

	void create_sampler(const CreateInfo& info);

	bool need_staging_buffer(const CreateInfo& info);

	bool has_stencil_component(VkFormat format);

	VkImageUsageFlagBits get_vk_usage(ImageUsage usage) const noexcept;

	static unsigned calc_mip_levels(unsigned width, unsigned height);

	static VkFilter get_vk_filter(Filter filter);

private:
	CreateInfo m_info;

	Device* p_device;
	VkDevice m_vk_device;

	VmaAllocator m_allocator;
	VmaAllocation m_allocation;

	ImageUsage m_usage;
	VkFormat m_format;

	VkImage m_image;

	VkImageView m_view;

	VkSampler m_sampler;
};

}}
