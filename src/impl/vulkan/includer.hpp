#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <algorithm>

#include <glslang/Public/ShaderLang.h>

#include "interface/shader.hpp"

namespace tgal {
namespace vulkan {

/* Default include class for normal include convention of search backward
 * through the stack of active include paths (for nested includes).
 * Can be overridden to customize.
 */
class DirStackFileIncluder : public glslang::TShader::Includer {
public:
	DirStackFileIncluder(ShaderReader* reader) : p_reader(reader) { }

	virtual ~DirStackFileIncluder() override { }

	virtual IncludeResult* includeLocal(
		const char* headerName,
		const char* includerName,
		size_t inclusionDepth
	) override
	{
		return read_local_path(headerName, includerName, (int)inclusionDepth);
	}

	virtual IncludeResult* includeSystem(
		const char* headerName,
		const char* includerName,
		size_t inclusionDepth
	) override
	{
		return read_system_path(headerName);
	}

	virtual void releaseInclude(IncludeResult* result) override
	{
		if (result != nullptr) {
			delete [] p_content;
			delete result;
		}
	}

	/* Externally set directories. E.g., from a command-line -I<dir>.
	 *  - Most-recently pushed are checked first.
	 *  - All these are checked after the parse-time stack of local
	 *    directories is checked.
	 *  - This only applies to the "local" form of #include.
	 *  - Makes its own copy of the path.
	 */
	virtual void push_extern_local_dir(const std::string& dir)
	{
		m_directory_stack.push_back(dir);
		m_external_local_directory_count = (int)m_directory_stack.size();
	}

protected:
	// Search for a valid "local" path based on combining the stack of include
	// directories and the nominal name of the header.
	IncludeResult* read_local_path(const char* headerName, const char* includerName, int depth)
	{
		// Discard popped include directories, and
		// initialize when at parse-time first level.
		m_directory_stack.resize(depth + m_external_local_directory_count);
		if (depth == 1)
			m_directory_stack.back() = get_directory(includerName);

		// Find a directory that works, using a reverse search of the include stack.
		for (auto it = m_directory_stack.rbegin(); it != m_directory_stack.rend(); ++it) {
			std::string path = *it + '/' + headerName;
			std::replace(path.begin(), path.end(), '\\', '/');
			if (p_reader->exists(path.c_str())) {
				m_directory_stack.push_back(get_directory(path));
				p_reader->open(path.c_str());
				p_content = new char[p_reader->length()];
				p_reader->read(p_content);
				p_reader->close();
				return new IncludeResult(
					path,
					p_content,
					p_reader->length(),
					p_content
				);
			}
		}

		return nullptr;
	}

	// Search for a valid <system> path.
	// Not implemented yet; returning nullptr signals failure to find.
	IncludeResult* read_system_path(const char* headerName)
	{
		return nullptr;
	}

	// If no path markers, return current working directory.
	// Otherwise, strip file name and return path leading up to it.
	static std::string get_directory(const std::string path)
	{
		size_t last = path.find_last_of("/\\");
		return last == std::string::npos ? "." : path.substr(0, last);
	}

protected:
	std::vector<std::string> m_directory_stack;
	int m_external_local_directory_count = 0;
	char* p_content;
	ShaderReader* p_reader;
};

}}
