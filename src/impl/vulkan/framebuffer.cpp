#include "framebuffer.hpp"
#include "render_pass.hpp"

namespace tgal {
namespace vulkan {

Framebuffer::Framebuffer(Device* device, const CreateInfo& info) :
	SwapchainDependant(info.swapchain),
	m_info(info),
	p_device(device),
	m_vk_device(device->vk_object())
{
	create();
}

Framebuffer::~Framebuffer()
{
	destroy();
}

void Framebuffer::create()
{
	m_buffers_count = 1;
	if (m_info.swapchain != nullptr) {
		m_buffers_count = m_info.swapchain->images_count();
		m_info.width = m_info.swapchain->extent().w;
		m_info.height = m_info.swapchain->extent().h;
	}
	m_framebuffers.resize(m_buffers_count);
	create_framebuffers(m_info);
}

void Framebuffer::destroy()
{
	for (auto& framebuffer : m_framebuffers) {
		vkDestroyFramebuffer(m_vk_device, framebuffer, nullptr);
	}
}

void Framebuffer::recreate(Swapchain* swapchain)
{
	// Don't recreate if the swapchain has the same number of images.
#if 0
	if (swapchain->images_count() == m_buffers_count)
		return;
#endif
	destroy();
	create();
}

void Framebuffer::create_framebuffers(const CreateInfo& info)
{
	auto render_pass = static_cast<RenderPass*>(info.render_pass);

	for (size_t i = 0; i < m_buffers_count; ++i) {

		std::vector<VkImageView> attachments;
		for (auto attachment : render_pass->attachments()) {
			attachments.push_back(attachment->vk_image_view(i));
		}

		VkFramebufferCreateInfo framebuffer_info = {};
		framebuffer_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebuffer_info.renderPass = render_pass->vk_object();
		framebuffer_info.attachmentCount = attachments.size();
		framebuffer_info.pAttachments = attachments.data();
		framebuffer_info.width = info.width;
		framebuffer_info.height = info.height;
		framebuffer_info.layers = 1;

		int res = vkCreateFramebuffer(
			m_vk_device,
			&framebuffer_info,
			nullptr, &m_framebuffers[i]
		);
		if (res != VK_SUCCESS) {
			throw std::runtime_error("Failed to create framebuffer!");
		}
	}
}

}}
