#pragma once

#include <vector>

#include "interface/buffer.hpp"
#include "swapchain.hpp"
#include "vk_mem_alloc.h"

namespace tgal {
namespace vulkan {

class Device;

class Buffer : public tgal::Buffer, public SwapchainDependant {
public:
	Buffer(Device* device, const CreateInfo& info);
	~Buffer();

	virtual void write(
		void* data,
		uint32_t size,
		uint32_t offset = 0
	) override;

	virtual void recreate(Swapchain* swapchain) override;

	void map(void** memory);
	void map(uint32_t index, void** memory);
	void unmap(uint32_t index = 0);

	VkBuffer vk_object(uint32_t index = 0) const
	{ return m_buffers[index]; }

	static void create_buffer(
		Device* device,
		uint32_t size,
		VkBufferUsageFlagBits usage,
		MemoryAccess access,
		VkSharingMode sharing_mode,
		VkBuffer* buffer,
		VmaAllocation* alloc
	);

	static void create_staging_buffer(
		Device* device,
		uint32_t size,
		const void* data,
		VkBuffer* buffer,
		VmaAllocation* alloc
	);

private:
	void create(const CreateInfo& info);
	void destroy();

	bool need_staging_buffer(const CreateInfo& info) const noexcept;

	void create_staging_buffer_and_copy_from_it(const CreateInfo& info);

	VkBufferUsageFlagBits get_vk_usage(BufferUsage usage) const noexcept;

private:
	CreateInfo m_info;

	Device* p_device;
	VkDevice m_vk_device;

	VmaAllocator m_allocator;
	std::vector<VmaAllocation> m_allocations;
	std::vector<VkBuffer> m_buffers;

	uint32_t m_buffers_count;
};

}}
