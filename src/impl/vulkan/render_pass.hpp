#pragma once

#include <vector>
#include <vulkan/vulkan.h>

#include "interface/render_pass.hpp"
#include "device.hpp"
#include "image.hpp"
#include "swapchain_dependant.hpp"

namespace tgal {
namespace vulkan {

class RenderPass : public tgal::RenderPass, public SwapchainDependant {
public:
	RenderPass(Device* device, const CreateInfo& info);
	~RenderPass();

	virtual void recreate(Swapchain* swapchain) override;

	VkRenderPass vk_object() const { return m_render_pass; }

	const std::vector<const ImageImpl*> attachments() const
	{ return m_attachments; };

private:
	void create(const CreateInfo& info);
	void destroy();

	VkAttachmentDescription create_attachment_desc(
		VkFormat format, bool depth_stencil);

private:
	CreateInfo m_info;

	Device* p_device;
	VkRenderPass m_render_pass;

	std::vector<const ImageImpl*> m_attachments;
};

}}
