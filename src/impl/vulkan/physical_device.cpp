#include <vector>
#include <set>

#include <vulkan/vulkan.hpp>

#include "physical_device.hpp"
#include "physical_device_selector.hpp"
#include "device.hpp"

namespace tgal {
namespace vulkan {

PhysicalDevice::PhysicalDevice(
	PhysicalDeviceSelector* selector,
	const char* name,
	const VkPhysicalDevice& device
) :
	p_selector(selector),
	m_name(name),
	m_physical_device(device),
	m_queue_families(p_selector->find_queue_families(device))
{
}

tgal::Device* PhysicalDevice::create_device()
{
	auto indices = p_selector->find_queue_families(m_physical_device);

	std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
	std::set<uint32_t> unique_queue_families = {
		static_cast<uint32_t>(indices.graphics),
		static_cast<uint32_t>(indices.present)
	};

	float queue_priority = 1.0f;

	for (uint32_t queue_family : unique_queue_families) {
		VkDeviceQueueCreateInfo queue_create_info = {};
		queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queue_create_info.queueFamilyIndex = queue_family;
		queue_create_info.queueCount = 1;
		queue_create_info.pQueuePriorities = &queue_priority;
		queue_create_infos.push_back(queue_create_info);
	}

	VkPhysicalDeviceFeatures device_features = {};
	device_features.samplerAnisotropy = VK_TRUE;

	VkDeviceCreateInfo create_info = {};
	create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	create_info.queueCreateInfoCount = queue_create_infos.size();
	create_info.pQueueCreateInfos = queue_create_infos.data();
	create_info.pEnabledFeatures = &device_features;

	auto extensions = p_selector->get_extensions();

	create_info.enabledExtensionCount = extensions.size();
	create_info.ppEnabledExtensionNames = extensions.data();

	auto layers = p_selector->get_validation_layers();
	create_info.enabledLayerCount = layers.size();
	create_info.ppEnabledLayerNames = layers.data();

	VkDevice device;
	if (vkCreateDevice(m_physical_device, &create_info, nullptr, &device)
			!= VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create logical device!");
	}

	VkQueue graphics_queue;
	vkGetDeviceQueue(device, indices.graphics, 0, &graphics_queue);
	VkQueue present_queue;
	vkGetDeviceQueue(device, indices.present, 0, &present_queue);

	return new Device(this, device, graphics_queue, present_queue);
}

}}
