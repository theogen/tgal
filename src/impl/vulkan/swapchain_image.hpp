#pragma once

#include <vector>
#include "image.hpp"

namespace tgal {
namespace vulkan {

class Swapchain;

class SwapchainImage : public ImageImpl {
public:
	SwapchainImage(Swapchain* swapchain)
		: ImageImpl(nullptr), p_swapchain(swapchain) {}

	virtual void recreate(Swapchain* swapchain) override {}

	virtual VkFormat vk_format() const override { return m_vk_format; }
	virtual uint32_t width() const noexcept override;
	virtual uint32_t height() const noexcept override;
	virtual ImageUsage usage() const override
	{ return ImageUsage::ColorAttachment; }

	virtual VkImageView vk_image_view(size_t i = 0) const override
	{ return m_image_views[i]; }

private:
	friend class Swapchain;

	const Swapchain* p_swapchain;

	VkFormat m_vk_format;
	std::vector<VkImage> m_images;
	std::vector<VkImageView> m_image_views;
};

}}
