#include <stdexcept>
#include <iostream>

#include "command_buffer.hpp"
#include "pipeline.hpp"
#include "swapchain.hpp"
#include "render_pass.hpp"
#include "buffer.hpp"
#include "descriptor_set.hpp"
#include "framebuffer.hpp"
#include "commands.hpp"

namespace tgal {
namespace vulkan {

CommandBuffer::CommandBuffer(
	Device* device,
	const CreateInfo& info,
	const VkCommandPool& pool
) :
	SwapchainDependant(info.swapchain),
	m_info(info),
	p_device(device),
	m_vk_device(device->vk_object()),
	m_pool(pool),
	m_one_time_submit(info.one_time_submit)
{
	create(info);
}

void CommandBuffer::record()
{
	for (auto command : m_recorded_commands) {
		command->record();
	}
}

void CommandBuffer::rerecord()
{
	begin();
	end();
}

void CommandBuffer::recreate(Swapchain* swapchain)
{
	destroy();
	create(m_info);
	rerecord();
}

void CommandBuffer::destroy()
{
	vkFreeCommandBuffers(
		m_vk_device,
		m_pool,
		static_cast<uint32_t>(m_buffers.size()),
		m_buffers.data()
	);
}

CommandBuffer::~CommandBuffer()
{
	for (auto command : m_recorded_commands)
		delete command;
	destroy();
}

void CommandBuffer::create(const CreateInfo& info)
{
	m_buffers_count = 1;
	if (info.swapchain != nullptr)
		m_buffers_count = info.swapchain->images_count();
	m_buffers.resize(m_buffers_count);
	allocate_buffers();
}

void CommandBuffer::allocate_buffers()
{
	VkCommandBufferAllocateInfo alloc_info = {};
	alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	alloc_info.commandPool = m_pool;
	alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	alloc_info.commandBufferCount = m_buffers_count;

	int res = vkAllocateCommandBuffers(
		m_vk_device, &alloc_info,
		m_buffers.data()
	);
	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to allocate command buffers!");
	}
}

void CommandBuffer::begin()
{
	for (auto& buffer : m_buffers) {
		VkCommandBufferBeginInfo begin_info = {};
		begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		if (m_one_time_submit)
			begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		else
			begin_info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
		begin_info.pInheritanceInfo = nullptr;

		int res = vkBeginCommandBuffer(buffer, &begin_info);
		if (res != VK_SUCCESS) {
			throw std::runtime_error("Failed to begin recording command buffer!");
		}
	}
}

void CommandBuffer::end()
{
	record();
	for (auto& buffer : m_buffers) {
		if (vkEndCommandBuffer(buffer) != VK_SUCCESS) {
			throw std::runtime_error("Failed to record command buffer!");
		}
	}
}

void CommandBuffer::begin_render_pass(const RenderPass::BeginInfo& info)
{
	m_recorded_commands.push_back(new Commands::BeginRenderPass(this, info));
}

void CommandBuffer::end_render_pass()
{
	m_recorded_commands.push_back(new Commands::EndRenderPass(this));
}

void CommandBuffer::bind_pipeline(tgal::Pipeline* pipeline)
{
	auto vk_pipeline = static_cast<Pipeline*>(pipeline);
	m_recorded_commands.push_back(
		new Commands::BindPipeline(this, vk_pipeline));
}

void CommandBuffer::copy_buffer(
	tgal::Buffer* src, tgal::Buffer* dest, uint32_t size)
{
	m_recorded_commands.push_back(new Commands::CopyBuffer(
		this,
		static_cast<Buffer*>(src),
		static_cast<Buffer*>(dest),
		size
	));
}

void CommandBuffer::bind_index_buffer(
	tgal::Buffer* index_buffer, tgal::IndexType type)
{
	m_recorded_commands.push_back(new Commands::BindIndexBuffer(
		this,
		static_cast<Buffer*>(index_buffer),
		type,
		0
	));
}

void CommandBuffer::bind_vertex_buffers(
	uint32_t first_binding, uint32_t binding_count, tgal::Buffer* buffer)
{
	m_recorded_commands.push_back(new Commands::BindVertexBuffer(
		this,
		first_binding,
		binding_count,
		static_cast<Buffer*>(buffer)
	));
}

void CommandBuffer::bind_descriptor_sets(
	uint32_t first_set, size_t count, tgal::DescriptorSet* sets)
{
	m_recorded_commands.push_back(new Commands::BindDescriptorSets(
		this,
		first_set,
		count,
		sets
	));
}

void CommandBuffer::set_viewport(Viewport viewport)
{
	m_recorded_commands.push_back(new Commands::SetViewport(this, viewport));
}

void CommandBuffer::set_scissor(Rect2D scissor)
{
	m_recorded_commands.push_back(new Commands::SetScissor(this, scissor));
}

void CommandBuffer::draw(
	uint32_t vertex_count,
	uint32_t instance_count,
	uint32_t first_vertex,
	uint32_t first_instance)
{
	m_recorded_commands.push_back(new Commands::Draw(
		this,
		vertex_count,
		instance_count,
		first_vertex,
		first_instance
	));
}

void CommandBuffer::draw_indexed(
	uint32_t index_count,
	uint32_t instance_count,
	uint32_t first_index,
	uint32_t vertex_offset,
	uint32_t first_instance)
{
	m_recorded_commands.push_back(new Commands::DrawIndexed(
		this,
		index_count,
		instance_count,
		first_index,
		vertex_offset,
		first_instance
	));
}

}}
