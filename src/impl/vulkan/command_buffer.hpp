#pragma once

#include <vector>

#include "interface/command_buffer.hpp"
#include "device.hpp"
#include "swapchain.hpp"
#include "pipeline.hpp"

namespace tgal {
namespace vulkan {

class CommandBuffer : public tgal::CommandBuffer, public SwapchainDependant {
public:
	struct Command {
		virtual ~Command() {}
		virtual void record() = 0;
	};

	CommandBuffer(
		Device* device,
		const CreateInfo& info,
		const VkCommandPool& pool
	);
	~CommandBuffer();

	virtual void recreate(Swapchain* swapchain) override;

	virtual void begin() override;
	virtual void end() override;

	virtual void begin_render_pass(const RenderPass::BeginInfo& info) override;
	virtual void end_render_pass() override;

	virtual void bind_pipeline(tgal::Pipeline* pipeline) override;

	void copy_buffer(
		tgal::Buffer* src, tgal::Buffer* dest, uint32_t size);

	virtual void bind_index_buffer(tgal::Buffer* buffer, tgal::IndexType type) override;

	virtual void bind_vertex_buffers(
		uint32_t first_binding,
		uint32_t binding_count,
		tgal::Buffer* buffer
	) override;

	virtual void bind_descriptor_sets(
		uint32_t first_set, size_t count, tgal::DescriptorSet* sets) override;

	virtual void set_viewport(Viewport viewport) override;
	virtual void set_scissor(Rect2D scissor) override;

	virtual void draw(
		uint32_t vertex_count,
		uint32_t instance_count,
		uint32_t first_vertex,
		uint32_t first_instance
	) override;

	virtual void draw_indexed(
		uint32_t index_count,
		uint32_t instance_count,
		uint32_t first_index,
		uint32_t vertex_offset,
		uint32_t first_instance
	) override;

	void set_current_pipeline(Pipeline* pipeline) noexcept
	{ p_current_pipeline = pipeline; }
	Pipeline* current_pipeline() const noexcept
	{ return p_current_pipeline; }

	VkCommandBuffer vk_object(uint32_t index = 0) const { return m_buffers[index]; }
	VkDevice vk_device() const { return m_vk_device; }
	uint32_t buffers_count() const { return m_buffers_count; }
	std::vector<VkCommandBuffer>& vk_command_buffers() { return m_buffers; }

private:
	void create(const CreateInfo& info);
	void record();
	void rerecord();
	void allocate_buffers();
	void destroy();
	void create_buffer(VkCommandBuffer* buffer);

private:
	CreateInfo m_info;
	Device* p_device;
	VkDevice m_vk_device;
	VkCommandPool m_pool;
	std::vector<VkCommandBuffer> m_buffers;
	std::vector<Command*> m_recorded_commands;
	uint32_t m_buffers_count;
	bool m_one_time_submit;
	Pipeline* p_current_pipeline;
};

}}
