#pragma once

#include <string>
#include <vulkan/vulkan.h>

#include "interface/device.hpp"
#include "interface/shader.hpp"
#include "command_buffer.hpp"

#include "swapchain_creator.hpp"

#include "vk_mem_alloc.h"

namespace tgal {
namespace vulkan {

class PhysicalDevice;

class Device : public tgal::Device {
public:
	Device(
		PhysicalDevice* physical_device,
		VkDevice device,
		VkQueue graphics_queue,
		VkQueue present_queue
	);
	~Device();

	/* Interface */

	virtual tgal::Swapchain* create_swapchain(
		const Swapchain::CreateInfo& info) override;

	virtual tgal::ShaderModule* create_shader_module(
		const ShaderModule::CreateInfo& info) override;

	virtual tgal::RenderPass* create_render_pass(
		const RenderPass::CreateInfo& info) override;

	virtual tgal::Framebuffer* create_framebuffer(
		const Framebuffer::CreateInfo& info) override;

	virtual tgal::Pipeline* create_pipeline(
		const Pipeline::CreateInfo& info) override;

	virtual tgal::CommandBuffer* create_command_buffer(
		const CommandBuffer::CreateInfo& info) override;

	CommandBuffer* begin_single_time_commands();

	virtual void submit_single_time_commands(tgal::CommandBuffer* buffer) override;

	virtual tgal::Buffer* create_buffer(
		const Buffer::CreateInfo& info) override;

	virtual tgal::Image* create_image(
		const Image::CreateInfo& info) override;

	virtual tgal::DescriptorSetLayout* create_descriptor_set_layout(
		const DescriptorSetLayout::CreateInfo& info) override;

	virtual tgal::DescriptorSet* create_descriptor_set(
		const DescriptorSet::CreateInfo& info) override;


	virtual void wait_idle() override { vkDeviceWaitIdle(m_device); };


	virtual void queue(tgal::CommandBuffer* commands) override;

	virtual void clear_queue() override;

	virtual void submit() override;

	/* Public methods */

	VkImageView create_image_view(
		VkImage image,
		VkFormat format,
		VkImageAspectFlags aspect_mask,
		uint32_t mip_levels
	);

	void increment_current_in_flight_frame() noexcept
	{ m_current_in_flight_frame = (m_current_in_flight_frame + 1) % m_max_frames_in_flight; }
	
	/* Getters */

	PhysicalDevice* physical_device() const { return p_physical_device; }
	VkDevice vk_object() const { return m_device; }

	VkQueue graphics_queue() const { return m_graphics_queue; }
	VkQueue present_queue() const { return m_present_queue; }

	VmaAllocator allocator() const { return m_allocator; }

	uint32_t max_frames_in_flight() const noexcept
	{ return m_max_frames_in_flight; }

	uint32_t current_in_flight_frame() const noexcept
	{ return m_current_in_flight_frame; }

	VkFence* in_flight_fence()
	{ return &m_in_flight_fences[m_current_in_flight_frame]; }

	VkSemaphore render_finished_semaphore()
	{ return m_render_finished_semaphores[m_current_in_flight_frame]; }

private:
	void create_command_pools();
	void create_sync_objects();

private:
	PhysicalDevice* p_physical_device;
	VkDevice m_device;
	VkQueue m_graphics_queue;
	VkQueue m_present_queue;

	SwapchainCreator m_swapchain_creator;
	VkCommandPool m_graphics_command_pool;

	VmaAllocator m_allocator;

	std::vector<VkCommandBuffer> m_queued_command_buffers;
	Swapchain* p_queued_command_buffers_swapchain;

	/* Sync objects */

	std::vector<VkSemaphore> m_render_finished_semaphores;
	std::vector<VkFence> m_in_flight_fences;

	/* Frames in-flight */

	const uint32_t m_max_frames_in_flight = 3;
	uint32_t m_current_in_flight_frame = 0;
};

}}
