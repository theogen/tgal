#pragma once

#include "interface/swapchain.hpp"

namespace tgal {
namespace vulkan {

class Swapchain;

class SwapchainDependant {
public:
	SwapchainDependant(tgal::Swapchain* swapchain);

	virtual ~SwapchainDependant();

	virtual void recreate(Swapchain* swapchain) = 0;

	virtual bool depends_on_swapchain() const
	{ return p_swapchain != nullptr; }

	void set_swapchain(Swapchain* swapchain);
	Swapchain* swapchain() const { return p_swapchain; }

private:
	Swapchain* p_swapchain;
};

}}
