#pragma once

#include <vector>
#include <vulkan/vulkan.h>
#include <SDL2/SDL.h>

#include "interface/instance.hpp"
#include "interface/physical_device.hpp"

#include "debug_messenger.hpp"
#include "window.hpp"
#include "physical_device.hpp"
#include "device.hpp"
#include "swapchain.hpp"

namespace tgal {
namespace vulkan {

class Instance : public tgal::Instance {
public:
	Instance(const CreateInfo& info);
	~Instance();

	/* Interface */

	// Physical device.
	virtual tgal::PhysicalDevice* pick_physical_device() override;

	// Window.
	virtual Window* window() const override { return p_window; }

	/* Public methods */

	// Whether validation layers are enabled.
	bool validation_layers_enabled() const
	{ return m_validation_layers.size() > 0; }
	// Validation layers.
	std::vector<const char*> validation_layers() const
	{ return m_validation_layers; }

	// VkInstance object.
	VkInstance vk_object() const { return m_instance; }

private:
	/* Private methods */

	// Create Vulkan instance.
	bool create_instance(const CreateInfo& info);
	// Get list of required extensions.
	std::vector<const char*> get_required_extensions(SDL_Window* window);
	// Check for suported extensions.
	bool check_extensions_support(const std::vector<const char*>& extensions);
	// Check for support for validation layers.
	bool check_validation_layer_support();

	// Create debug messenger.
	void create_debug_messenger();

private:
	Window* p_window;
	VkInstance m_instance;
	DebugMessenger* p_debug_messenger;
	PhysicalDeviceSelector* m_physical_device_selector;

	// Debug.

	std::vector<const char*> m_validation_layers = {
#ifndef NDEBUG
		"VK_LAYER_KHRONOS_validation"
#endif
	};
};

}}
