#include <iostream>
#include <stdexcept>
#include <vector>
#include <set>

#include "physical_device_selector.hpp"
#include "instance.hpp"
#include "swapchain_creator.hpp"

namespace tgal {
namespace vulkan {

PhysicalDeviceSelector::PhysicalDeviceSelector(
	Instance* instance
) :
	m_instance(instance->vk_object()),
	m_surface(instance->window()->surface),
	m_validation_layers(instance->validation_layers())
{
}

PhysicalDevice* PhysicalDeviceSelector::pick_device()
{
	// Physical devices count.
	uint32_t device_count = 0;
	vkEnumeratePhysicalDevices(m_instance, &device_count, nullptr);

	// No Vulkan support!
	if (device_count == 0) {
		throw std::runtime_error("Failed to find any GPU with Vulkan support!");
	}

	// Obtaining list of devices.
	std::vector<VkPhysicalDevice> devices(device_count);
	vkEnumeratePhysicalDevices(m_instance, &device_count, devices.data());

	// Print available devices.
	std::cerr << "Available devices:\n";
	for (const VkPhysicalDevice& device : devices) {
		VkPhysicalDeviceProperties props;
		vkGetPhysicalDeviceProperties(device, &props);
		std::cerr << "- " << props.deviceName << '\n';
	}
	std::cerr << '\n';

	// Checking for the suitablility.
	for (const VkPhysicalDevice& device : devices) {
		if (is_device_suitable(device)) {
			VkPhysicalDeviceProperties props;
			vkGetPhysicalDeviceProperties(device, &props);
			std::cerr << "Using " << props.deviceName << "\n\n";

			return new PhysicalDevice(this, props.deviceName, device);
		}
	}

	throw std::runtime_error("None of GPUs are suitable!");

	return nullptr;
}

bool PhysicalDeviceSelector::is_device_suitable(const VkPhysicalDevice& device)
{
	QueueFamilyIndices indices = find_queue_families(device);
	bool extensions_supported = check_extension_support(device);

	bool swap_chain_adequate = false;
	if (extensions_supported) {
		SwapchainSupportDetails swap_chain_support
			= SwapchainCreator::query_support(device, m_surface);

		swap_chain_adequate =
			!swap_chain_support.formats.empty() &&
			!swap_chain_support.present_modes.empty();
	}

	return indices.is_complete() &&
		extensions_supported &&
		swap_chain_adequate;
}

bool PhysicalDeviceSelector::check_extension_support(
		const VkPhysicalDevice& device)
{
	uint32_t extension_count;
	vkEnumerateDeviceExtensionProperties(
			device, nullptr, &extension_count, nullptr);

	std::vector<VkExtensionProperties> available_extensions(extension_count);
	vkEnumerateDeviceExtensionProperties(
			device, nullptr, &extension_count, available_extensions.data());

	std::set<std::string> required_extensions(
			device_extensions.begin(), device_extensions.end());

	for (const auto& extension : available_extensions) {
		required_extensions.erase(extension.extensionName);
	}

	return required_extensions.empty();
}

QueueFamilyIndices PhysicalDeviceSelector::find_queue_families(
		const VkPhysicalDevice& physical_device)
{
	QueueFamilyIndices indices;

	uint32_t queue_family_count = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(
			physical_device, &queue_family_count, nullptr);

	std::vector<VkQueueFamilyProperties> queue_families(queue_family_count);
	vkGetPhysicalDeviceQueueFamilyProperties(
			physical_device, &queue_family_count, queue_families.data());

	int i = 0;
	for (const auto& queue_family : queue_families) {
		if (queue_family.queueCount > 0 &&
			queue_family.queueFlags & VK_QUEUE_GRAPHICS_BIT)
		{
			indices.graphics = i;
		}

		VkBool32 present_support = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(
				physical_device, i, m_surface, &present_support);

		if (queue_family.queueCount > 0 && present_support)
			indices.present = i;

		if (indices.is_complete())
			break;

		++i;
	}

	return indices;
}

VkSampleCountFlagBits PhysicalDeviceSelector::get_max_usable_sample_count(
	const VkPhysicalDevice& device)
{
	VkPhysicalDeviceProperties properties;
	vkGetPhysicalDeviceProperties(device, &properties);

	VkSampleCountFlags counts
		= properties
			.limits
			.framebufferColorSampleCounts
		& properties
			.limits
			.framebufferDepthSampleCounts;

	if (counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
	if (counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
	if (counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
	if (counts & VK_SAMPLE_COUNT_8_BIT)  { return VK_SAMPLE_COUNT_8_BIT; }
	if (counts & VK_SAMPLE_COUNT_4_BIT)  { return VK_SAMPLE_COUNT_4_BIT; }
	if (counts & VK_SAMPLE_COUNT_2_BIT)  { return VK_SAMPLE_COUNT_2_BIT; }

	return VK_SAMPLE_COUNT_1_BIT;
}

}}
