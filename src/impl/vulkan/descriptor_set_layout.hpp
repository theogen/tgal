#pragma once

#include <vulkan/vulkan.h>
#include "interface/descriptor_set_layout.hpp"

namespace tgal {
namespace vulkan {

class Device;

class DescriptorSetLayout : public tgal::DescriptorSetLayout {
public:
	DescriptorSetLayout(Device* device, const CreateInfo& info);
	~DescriptorSetLayout();

	virtual const std::vector<Binding>& bindings() const
	{ return m_bindings; }

	VkDescriptorSetLayout vk_object() const
	{ return m_descriptor_set_layout; }

	static VkDescriptorType get_vk_type(DescriptorType type);

private:
	VkDevice m_vk_device;
	VkDescriptorSetLayout m_descriptor_set_layout;
	std::vector<Binding> m_bindings;
};

}}
