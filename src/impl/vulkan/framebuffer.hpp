#include <vulkan/vulkan.hpp>
#include "interface/framebuffer.hpp"
#include "swapchain.hpp"

namespace tgal {
namespace vulkan {

class Device;

class Framebuffer : public tgal::Framebuffer, public SwapchainDependant {
public:
	Framebuffer(Device* device, const CreateInfo& info);
	~Framebuffer();

	VkFramebuffer vk_object(size_t i = 0) const { return m_framebuffers[i]; }

	virtual void recreate(Swapchain* swapchain) override;

private:
	void create();
	void destroy();
	void create_framebuffers(const CreateInfo& info);

private:
	CreateInfo m_info;
	Device* p_device;
	VkDevice m_vk_device;
	size_t m_buffers_count;

	std::vector<VkFramebuffer> m_framebuffers;
};

}}
