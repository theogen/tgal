#include <stdexcept>

#include "shader.hpp"
#include "descriptor_set_layout.hpp"
#include "device.hpp"

namespace tgal {
namespace vulkan {

DescriptorSetLayout::DescriptorSetLayout(
	Device* device,
	const CreateInfo& info
) :
	m_vk_device(device->vk_object()),
	m_bindings(info.bindings)
{
	// Descriptor set layout bindings.
	auto& bindings = info.bindings;
	std::vector<VkDescriptorSetLayoutBinding> vk_bindings;
	vk_bindings.reserve(bindings.size());

	for (auto& binding : bindings) {
		VkDescriptorSetLayoutBinding vk_binding = {};
		vk_binding.binding = binding.binding;
		vk_binding.descriptorCount = binding.count;
		vk_binding.descriptorType = get_vk_type(binding.type);
		vk_binding.pImmutableSamplers = nullptr;
		vk_binding.stageFlags = ShaderModule::vk_shader_stage(binding.stages);

		vk_bindings.push_back(vk_binding);
	}

	// Descriptor set layout.
	VkDescriptorSetLayoutCreateInfo desc_set_layout_info = {};
	desc_set_layout_info.sType
		= VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	desc_set_layout_info.bindingCount = vk_bindings.size();
	desc_set_layout_info.pBindings = vk_bindings.data();

	int res = vkCreateDescriptorSetLayout(
		device->vk_object(),
		&desc_set_layout_info,
		nullptr,
		&m_descriptor_set_layout
	);
	if (res != VK_SUCCESS) {
		throw std::runtime_error("Failed to create descriptor set layout!");
	}
}

DescriptorSetLayout::~DescriptorSetLayout()
{
	vkDestroyDescriptorSetLayout(m_vk_device, m_descriptor_set_layout, nullptr);
}

VkDescriptorType DescriptorSetLayout::get_vk_type(DescriptorType type)
{
	switch (type) {
		case DescriptorType::CombinedImageSampler:
			return VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		case DescriptorType::UniformBuffer:
			return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	}

	return VK_DESCRIPTOR_TYPE_MAX_ENUM;
}

}}
