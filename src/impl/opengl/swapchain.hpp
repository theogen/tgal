#pragma once

#include "opengl.h"
#include "interface/swapchain.hpp"

namespace tgal {
namespace opengl {

class Window;

class Swapchain : public tgal::Swapchain {
public:
	Swapchain(const CreateInfo& info);
	~Swapchain();

	//! Presents image.
	virtual const tgal::Image* image() const override { return nullptr; }
	virtual Extent extent() const override
	{
		GLint dims[4] = {0};
		glGetIntegerv(GL_VIEWPORT, dims);
		GLint w = dims[2];
		GLint h = dims[3];
		return Extent(w, h);
	}
	virtual uint32_t images_count() const override { return 1; }

	virtual void acquire_next_image() override {}
	virtual void present() override;

	virtual uint32_t current_image_index() const override
	{ return 0; }

#if 0
	//! Set FPS limit (0 for unlimited).
	virtual void set_fps_limit(float limit) override { m_fps_limit = limit; };
	//! Returns current FPS limit (0 for unlimited).
	virtual float fps_limit() const noexcept override { return m_fps_limit; }
	//! Returns current FPS.
	virtual float fps() const noexcept override { return 0; }

	//! Returns delta time.
	virtual float delta() const noexcept override { return m_delta; }

	virtual int width() const noexcept override { return 0; }
	virtual int height() const noexcept override { return 0; }
#endif

private:
	Window* p_window;
	float m_fps_limit = 0;
	float m_delta = 0;
};

}}
