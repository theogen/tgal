#pragma once

#include "interface/physical_device.hpp"
#include "interface/device.hpp"

namespace tgal {
namespace opengl {

class PhysicalDevice : public tgal::PhysicalDevice {
public:
	virtual tgal::Device* create_device() override;
	virtual const char* name() const noexcept override
	{ return "Default"; }
};
	
}}
