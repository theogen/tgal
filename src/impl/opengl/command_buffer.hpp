#pragma once

#include "interface/command_buffer.hpp"

namespace tgal {
namespace opengl {

class Pipeline;

class CommandBuffer : public tgal::CommandBuffer {
public:
	struct Command {
		virtual ~Command() {}
		virtual void execute() = 0;
	};

	~CommandBuffer();

	virtual void begin() override;
	virtual void end() override;

	virtual void begin_render_pass(const RenderPass::BeginInfo& info) override;
	virtual void end_render_pass() override;

	virtual void bind_pipeline(tgal::Pipeline* pipeline) override;

	virtual void bind_index_buffer(tgal::Buffer* buffer, tgal::IndexType type) override;

	virtual void bind_vertex_buffers(
		uint32_t first_binding,
		uint32_t binding_count,
		tgal::Buffer* buffer
	) override;

	virtual void bind_descriptor_sets(
		uint32_t first_set, size_t count, tgal::DescriptorSet* sets) override;

	virtual void set_viewport(Viewport viewport) override { }
	virtual void set_scissor(Rect2D scissor) override { }

	virtual void draw(
		uint32_t vertex_count,
		uint32_t instance_count,
		uint32_t first_vertex,
		uint32_t first_instance
	) override;

	virtual void draw_indexed(
		uint32_t index_count,
		uint32_t instance_count,
		uint32_t first_index,
		uint32_t vertex_offset,
		uint32_t first_instance
	) override;

	void execute();

	void set_current_pipeline(Pipeline* pipeline) noexcept { p_current_pipeline = pipeline; }
	Pipeline* current_pipeline() const noexcept { return p_current_pipeline; }

private:
	Pipeline* p_current_pipeline;
	std::vector<Command*> m_recorded_commands;
};

}}
