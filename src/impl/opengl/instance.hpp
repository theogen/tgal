#pragma once

#include <SDL2/SDL.h>
#include "interface/instance.hpp"

namespace tgal {
namespace opengl {

class Device;
class Window;
class PhysicalDevice;

class Instance : public tgal::Instance {
public:
	Instance(const CreateInfo& info);
	~Instance();

	virtual tgal::PhysicalDevice* pick_physical_device() override;

	virtual tgal::Window* window() const override;

private:
	void set_gl_attributes();
	void init_gl_context();

private:
	SDL_GLContext m_context;
	Window* p_window;
	PhysicalDevice* p_physical_device;
};

}}
