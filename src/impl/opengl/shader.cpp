#include "shader.hpp"

namespace tgal {
namespace opengl {

GLenum ShaderModule::get_gl_stage(ShaderStage::Stage stage)
{
	switch (stage) {
		case ShaderStage::Vert:
			return GL_VERTEX_SHADER;
		case ShaderStage::Tesc:
			return GL_TESS_CONTROL_SHADER;
		case ShaderStage::Tese:
			return GL_TESS_EVALUATION_SHADER;
		case ShaderStage::Geom:
			return GL_GEOMETRY_SHADER;
		case ShaderStage::Frag:
			return GL_FRAGMENT_SHADER;
		case ShaderStage::Comp:
			return GL_COMPUTE_SHADER;
	}

	return 0;
}

}}
