#include <iostream>

#include "swapchain.hpp"
#include "window.hpp"
#include "command_buffer.hpp"

namespace tgal {
namespace opengl {

Swapchain::Swapchain(const CreateInfo& info)
{
	p_window = static_cast<Window*>(info.window);
}

Swapchain::~Swapchain()
{
}

void Swapchain::present()
{
	SDL_GL_SwapWindow(p_window->sdl_window());
}

}}
