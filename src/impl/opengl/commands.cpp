#include "commands.hpp"
#include "pipeline.hpp"

namespace tgal {
namespace opengl {
namespace Commands {
	
void BeginRenderPass::execute()
{
	glClearColor(
		m_info.clear_color.r,
		m_info.clear_color.g,
		m_info.clear_color.b,
		m_info.clear_color.a
	);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void BindPipeline::execute()
{
	p_buffer->set_current_pipeline(p_pipeline);
	p_buffer->current_pipeline()->bind();
}

void BindIndexBuffer::execute()
{
	p_buffer->current_pipeline()->bind_index_buffer(p_index_buffer, m_type);
}

void BindVertexBuffer::execute()
{
	p_buffer->current_pipeline()->bind_vertex_buffers(m_first_binding, m_bindings_count, p_vertex_buffers);
}

void BindDescriptorSets::execute()
{
	p_buffer->current_pipeline()->bind_descriptor_sets(m_first_set, m_count, p_sets);
}

void Draw::execute()
{
}

void DrawIndexed::execute()
{
	p_buffer->current_pipeline()->draw(m_index_count);
}

}}}
