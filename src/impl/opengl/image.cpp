#include <stdexcept>

#include "image.hpp"
#include "format.hpp"

namespace tgal {
namespace opengl {

Image::Image(const CreateInfo& info)
{
	m_target = GL_TEXTURE_2D;

	// Creating new texture.
	glGenTextures(1, &m_texture_id);
	// Binding the texture.
	glBindTexture(m_target, m_texture_id);

	// Choosing format.
	FormatInfo format_info = get_format_info(info.format);
	GLint intern_format = format_info.intern_format;
	GLint format = format_info.format;

	// Loading texture.
	glTexImage2D(
		m_target,                // Target
		0,                       // Level
		intern_format,           // Internal format
		info.width, info.height, // Size
		0,                       // Border
		format,                  // Format
		GL_UNSIGNED_BYTE,        // Type
		info.pixels              // Pixel data
	);

	// Enable filtering.
	glTexParameteri(m_target, GL_TEXTURE_MAG_FILTER, get_gl_filter(info.mag_filter));
	glTexParameteri(m_target, GL_TEXTURE_MIN_FILTER, get_gl_filter(info.min_filter));

	// Mipmaps.
	if (info.gen_mipmap)
		glGenerateMipmap(GL_TEXTURE_2D);
}

Image::~Image()
{
	glDeleteTextures(1, &m_texture_id);
}

GLint Image::get_gl_filter(Filter filter)
{
	switch (filter) {
		case Filter::Nearest:
			return GL_NEAREST;
		case Filter::Linear:
			return GL_LINEAR;
	}

	throw std::runtime_error("Unknown filter specified.");
}

}}
