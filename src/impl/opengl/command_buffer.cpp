#include "command_buffer.hpp"
#include "pipeline.hpp"
#include "commands.hpp"
#include "buffer.hpp"
#include "descriptor_set.hpp"

namespace tgal {
namespace opengl {

CommandBuffer::~CommandBuffer()
{
	for (Command* command : m_recorded_commands)
		delete command;
}

void CommandBuffer::begin()
{
}

void CommandBuffer::end()
{
}

void CommandBuffer::begin_render_pass(const RenderPass::BeginInfo& info)
{
	m_recorded_commands.push_back(new Commands::BeginRenderPass(this, info));
}

void CommandBuffer::end_render_pass()
{
}

void CommandBuffer::bind_pipeline(tgal::Pipeline* pipeline)
{
	m_recorded_commands.push_back(new Commands::BindPipeline(
		this, dynamic_cast<Pipeline*>(pipeline)));
	set_current_pipeline(dynamic_cast<Pipeline*>(pipeline));
	current_pipeline()->bind();
}

void CommandBuffer::bind_index_buffer(tgal::Buffer* buffer, tgal::IndexType type)
{
	m_recorded_commands.push_back(new Commands::BindIndexBuffer(this, dynamic_cast<Buffer*>(buffer), type));
	current_pipeline()->bind_index_buffer(dynamic_cast<Buffer*>(buffer), type);
}

void CommandBuffer::bind_vertex_buffers(
	uint32_t first_binding, uint32_t binding_count, tgal::Buffer* buffer)
{
	m_recorded_commands.push_back(new Commands::BindVertexBuffer(this, first_binding, binding_count, dynamic_cast<Buffer*>(buffer)));
	current_pipeline()->bind_vertex_buffers(first_binding, binding_count, dynamic_cast<Buffer*>(buffer));
}

void CommandBuffer::bind_descriptor_sets(
	uint32_t first_set, size_t count, tgal::DescriptorSet* sets)
{
	m_recorded_commands.push_back(new Commands::BindDescriptorSets(
		this,
		first_set,
		count,
		dynamic_cast<DescriptorSet*>(sets))
	);
}

void CommandBuffer::draw(
	uint32_t vertex_count,
	uint32_t instance_count,
	uint32_t first_vertex,
	uint32_t first_instance)
{
}

void CommandBuffer::draw_indexed(
	uint32_t index_count,
	uint32_t instance_count,
	uint32_t first_index,
	uint32_t vertex_offset,
	uint32_t first_instance)
{
	m_recorded_commands.push_back(new Commands::DrawIndexed(this, index_count, instance_count));
}

void CommandBuffer::execute()
{
	for (auto& command : m_recorded_commands)
		command->execute();
}

}}
