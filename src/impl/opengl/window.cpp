#include <iostream>
#include <string>
#include <SDL2/SDL_vulkan.h>

#include "window.hpp"

namespace tgal {
namespace opengl {

Window::Window(const CreateInfo& info)
	: m_flags(info.flags)
{
	p_window = initialize_sdl(info);
}

Window::~Window()
{
	SDL_DestroyWindow(p_window);
	SDL_Quit();
}

SDL_Window* Window::initialize_sdl(const CreateInfo& info)
{
	// Print SDL version.
	print_sdl_info();

	// Initialize video.
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		throw std::runtime_error(
			std::string("Failed to initialize SDL: ") + SDL_GetError()
		);
	}

	// Create window.
	SDL_Window* window = SDL_CreateWindow(
		info.title,
		info.x, info.y,
		info.w, info.h,
		SDL_WINDOW_OPENGL | m_flags
	);
	if (!window) {
		throw std::runtime_error(
			std::string("Couldn't create window: ") + SDL_GetError()
		);
	}

	// Return.
	return window;
}

void Window::print_sdl_info()
{
	// Compiled version.
	SDL_version compiled;
	SDL_VERSION(&compiled);
	printf("Compiled against SDL %d.%d.%d.\n",
		   compiled.major, compiled.minor, compiled.patch);

	// Linked version.
	SDL_version linked;
	SDL_GetVersion(&linked);
	printf("Linked against SDL %d.%d.%d.\n\n",
		   linked.major, linked.minor, linked.patch);
}

}}
