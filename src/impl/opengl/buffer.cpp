#include "buffer.hpp"

namespace tgal {
namespace opengl {

Buffer::Buffer(const CreateInfo& info)
{
	GLenum usage;
	switch(info.usage) {
		case BufferUsage::IndexBuffer:
			m_target = GL_ELEMENT_ARRAY_BUFFER;
			usage = GL_STATIC_DRAW;
			break;
		case BufferUsage::VertexBuffer:
			m_target = GL_ARRAY_BUFFER;
			usage = GL_STATIC_DRAW;
			break;
		case BufferUsage::UniformBuffer:
			m_target = GL_UNIFORM_BUFFER;
			usage = GL_DYNAMIC_DRAW;
			break;
	}

	glGenBuffers(1, &m_buffer);
	glBindBuffer(m_target, m_buffer);
	glBufferData(
		m_target,
		info.size,
		info.data,
		usage
	);
	glBindBuffer(m_target, 0);
}

Buffer::~Buffer()
{
	glDeleteBuffers(1, &m_buffer);
}

void Buffer::write(
	void* data,
	uint32_t size,
	uint32_t offset)
{
	glBindBuffer(m_target, m_buffer);
	glBufferSubData(
		m_target,
		offset,
		size,
		data
	);
	glBindBuffer(m_target, 0);
}

}}
