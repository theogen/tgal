#pragma once

#include <string>
#include "opengl.h"
#include "interface/shader.hpp"

namespace tgal {
namespace opengl {

class ShaderModule : public tgal::ShaderModule {
public:
	ShaderModule(const CreateInfo& info) : m_info(info) {}

	const CreateInfo& info() const noexcept { return m_info; }

	static GLenum get_gl_stage(ShaderStage::Stage stage);

private:
	CreateInfo m_info;
};

}}
