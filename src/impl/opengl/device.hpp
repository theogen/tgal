#pragma once

#include "interface/device.hpp"

namespace tgal {
namespace opengl {

class CommandBuffer;

class Device : public tgal::Device {
public:
	Device();

	/* Creation methods */

	virtual tgal::Swapchain* create_swapchain(
		const tgal::Swapchain::CreateInfo& info) override;

	virtual tgal::ShaderModule* create_shader_module(
		const tgal::ShaderModule::CreateInfo& info) override;

	virtual tgal::RenderPass* create_render_pass(
		const tgal::RenderPass::CreateInfo& info) override { return nullptr; }

	virtual tgal::Framebuffer* create_framebuffer(
		const tgal::Framebuffer::CreateInfo& info) override { return nullptr; }

	virtual tgal::DescriptorSetLayout* create_descriptor_set_layout(
		const tgal::DescriptorSetLayout::CreateInfo& info) override;

	virtual tgal::DescriptorSet* create_descriptor_set(
		const tgal::DescriptorSet::CreateInfo& info) override;

	virtual tgal::Pipeline* create_pipeline(
		const tgal::Pipeline::CreateInfo& info) override;

	virtual tgal::CommandBuffer* create_command_buffer(
		const tgal::CommandBuffer::CreateInfo& info) override;

	virtual tgal::Buffer* create_buffer(
		const tgal::Buffer::CreateInfo& info) override;

	virtual tgal::Image* create_image(
		const tgal::Image::CreateInfo& info) override;

	virtual void wait_idle() override {}

	virtual void submit_single_time_commands(
		tgal::CommandBuffer* buffer) override { }

	virtual void queue(tgal::CommandBuffer* commands) override;

	virtual void clear_queue() override;

	virtual void submit() override;

private:
	std::vector<CommandBuffer*> m_queued_command_buffers;
};

}}
