#include "device.hpp"
#include "opengl.h"

#include "swapchain.hpp"
#include "pipeline.hpp"
#include "buffer.hpp"
#include "image.hpp"
#include "shader.hpp"
#include "command_buffer.hpp"
#include "descriptor_set_layout.hpp"
#include "descriptor_set.hpp"

namespace tgal {
namespace opengl {

Device::Device()
{
	glEnable(GL_DEPTH_TEST);
}

/* Creation methods */

tgal::Swapchain* Device::create_swapchain(const Swapchain::CreateInfo& info)
{
	return new Swapchain(info);
}

tgal::Buffer* Device::create_buffer(const Buffer::CreateInfo& info)
{
	return new Buffer(info);
}

tgal::Image* Device::create_image(
	const Image::CreateInfo& info)
{
	return new Image(info);
}

tgal::ShaderModule* Device::create_shader_module(const ShaderModule::CreateInfo& info)
{
	return new ShaderModule(info);
}

tgal::Pipeline* Device::create_pipeline(const Pipeline::CreateInfo& info)
{
	return new Pipeline(this, info);
}

tgal::DescriptorSetLayout* Device::create_descriptor_set_layout(
	const DescriptorSetLayout::CreateInfo& info)
{
	return new DescriptorSetLayout(info);
}

tgal::DescriptorSet* Device::create_descriptor_set(
	const DescriptorSet::CreateInfo& info)
{
	return new DescriptorSet(info);
}

tgal::CommandBuffer* Device::create_command_buffer(
	const CommandBuffer::CreateInfo& info)
{
	return new CommandBuffer();
}

void Device::queue(tgal::CommandBuffer* commands)
{
	m_queued_command_buffers.push_back(dynamic_cast<CommandBuffer*>(commands));
}

void Device::clear_queue()
{
	m_queued_command_buffers.clear();
}

void Device::submit()
{
	for (CommandBuffer* commands : m_queued_command_buffers)
		commands->execute();
}

}}
