#pragma once

#include "interface/buffer.hpp"
#include "opengl.h"

namespace tgal {
namespace opengl {

class Buffer : public tgal::Buffer {
public:
	Buffer(const CreateInfo& info);
	~Buffer();

	virtual void write(
		void* data,
		uint32_t size,
		uint32_t offset
	) override;

	unsigned id() const noexcept { return m_buffer; }
	
private:
	unsigned m_buffer;
	GLenum m_target;
};

}}
