#include <stdexcept>

#include "opengl.h"
#include "instance.hpp"
#include "window.hpp"
#include "swapchain.hpp"
#include "device.hpp"
#include "physical_device.hpp"

namespace tgal {
namespace opengl {

Instance::Instance(const CreateInfo& info)
{
	p_window = dynamic_cast<Window*>(info.window);
	p_physical_device = new PhysicalDevice();
	init_gl_context();
}

Instance::~Instance()
{
	delete p_physical_device;
	SDL_GL_DeleteContext(m_context);
}

tgal::PhysicalDevice* Instance::pick_physical_device()
{
	return p_physical_device;
}

tgal::Window* Instance::window() const
{
	return p_window;
}

void Instance::set_gl_attributes()
{
	SDL_GL_SetAttribute(
		SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
}

void Instance::init_gl_context()
{
	set_gl_attributes();

	m_context = SDL_GL_CreateContext(p_window->sdl_window());

	if (m_context == nullptr) {
		throw std::runtime_error("Couldn't create OpenGL context!\n");
	}

#ifndef __APPLE__
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		throw std::runtime_error("Couldn't initialize GLEW!\n");
	}
	if (!GLEW_VERSION_2_1) {
		throw std::runtime_error("GLEW 2.1 required!\n");
	}
#endif
}

}}
