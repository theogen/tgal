#pragma once

#include <vector>
#include "interface/descriptor_set_layout.hpp"

namespace tgal {
namespace opengl {

struct DescriptorSetLayout : public tgal::DescriptorSetLayout {
	DescriptorSetLayout(const CreateInfo& info);

	virtual const std::vector<Binding>& bindings() const override
	{ return m_info.bindings; }

private:
	CreateInfo m_info;
};

}}
