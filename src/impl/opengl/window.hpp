#pragma once

#include <SDL2/SDL.h>
#include "interface/window.hpp"

namespace tgal {
namespace opengl {

class Window : public tgal::Window {
public:
	Window(const CreateInfo& info);
	~Window();

	virtual SDL_Window* sdl_window() const noexcept override
	{ return p_window; }

private:
	SDL_Window* initialize_sdl(const CreateInfo& info);
	void print_sdl_info();

private:
	int m_w, m_h, m_x, m_y;
	uint32_t m_flags;
	SDL_Window* p_window = nullptr;
};

}}
