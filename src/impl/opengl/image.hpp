#pragma once

#include "interface/image.hpp"
#include "opengl.h"

namespace tgal {
namespace opengl {

class Image : public tgal::Image {
public:
	Image(const CreateInfo& info);
	~Image();

	virtual uint32_t width() const noexcept override
	{ return info.width; }
	virtual uint32_t height() const noexcept override
	{ return info.height; }
	virtual ImageUsage usage() const noexcept override
	{ return info.usage; }

	uint32_t id() const noexcept { return m_texture_id; }
	GLenum target() const noexcept { return m_target; }

private:
	static GLint get_gl_filter(Filter filter);

private:
	CreateInfo info;

	uint32_t m_texture_id;
	GLenum m_target;
};

}}
