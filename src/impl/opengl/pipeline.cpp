#include "pipeline.hpp"

#include <cstring>
#include <fstream>
#include <iostream>
#include <stdexcept>

#include "format.hpp"
#include "buffer.hpp"
#include "shader.hpp"
#include "image.hpp"
#include "descriptor_set.hpp"
#include "descriptor_set_layout.hpp"

namespace tgal {
namespace opengl {

Pipeline::Pipeline(Device* device, const CreateInfo& info)
	: m_info(info)
{
	// Creating VAO.
	glGenVertexArrays(1, &m_vertex_array);

	// Loading shaders.
	for (auto& stage : info.stages) {
		auto module = dynamic_cast<ShaderModule*>(stage.module);
		std::cout << "Loading shader: " << module->info().path << "\n\n";
		ShaderReader* reader = module->info().reader;
		reader->open(module->info().path.c_str());
		char* source = new char[reader->length() + 1];
		reader->read(source);
		source[reader->length()] = '\0';
		load_stage(source, reader->length(), stage);
		reader->close();
		delete [] source;
	}
	link_shader();
}

Pipeline::~Pipeline()
{
	glDeleteVertexArrays(1, &m_vertex_array);

	// Check if this shader has been initialized.
	if (m_program_id == (unsigned)-1)
		return;

	// Stopping the shader.
	glUseProgram(0);

	// Deleting individual stages.
	for (size_t i = 0; i < m_stages_id.size(); ++i) {
		if (m_stages_id[i] != (unsigned)-1) {
			glDetachShader(m_program_id, m_stages_id[i]);
			glDeleteShader(m_stages_id[i]);
		}
	}

	// Deleting program.
	glDeleteProgram(m_program_id);
}

void Pipeline::bind()
{
	glBindVertexArray(m_vertex_array);
}

void Pipeline::bind_index_buffer(Buffer* buffer, IndexType index_type)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->id());
	switch (index_type) {
		case IndexType::UInt16:
			m_index_type = GL_UNSIGNED_SHORT;
			break;
		case IndexType::UInt32:
			m_index_type = GL_UNSIGNED_INT;
			break;
	}
}

void Pipeline::bind_vertex_buffers(uint32_t first_binding, uint32_t binding_count, Buffer* buffer)
{
	for (size_t i = 0; i < binding_count; ++i) {
		glBindBuffer(GL_ARRAY_BUFFER, buffer->id());

		for (uint32_t j = first_binding; j < first_binding + binding_count; ++j) {
			auto binding = m_info.vertex_input.bindings[j];

			for (const auto& attrib : m_info.vertex_input.attributes)
			{
				if (attrib.binding != j)
					continue;
				FormatInfo fmt = get_format_info(attrib.format);
				glVertexAttribPointer(
					attrib.location,
					fmt.size,
					fmt.type,
					fmt.normalized,
					binding.stride,
					reinterpret_cast<void*>(attrib.offset)
				);
				glVertexAttribDivisor(
					attrib.location,
					binding.rate == VertexInput::Rate::Vertex ? 0 : 1
				);
			}
		}
	}
}

void Pipeline::bind_descriptor_sets(
	uint32_t first_set, uint32_t count, DescriptorSet* sets)
{
	// Enumerating through layouts.
	for (uint32_t i = first_set; i < first_set + count; ++i) {
		const tgal::DescriptorSetLayout& layout = *m_info.layout.layouts[i];
		const DescriptorSet& set = sets[i - first_set];
		// Enumerating through bindings.
		for (uint32_t j = 0; j < layout.bindings().size(); ++j) {
			const auto& binding = layout.bindings()[j];
			const auto& write = set.writes()[j];
			if (binding.type == DescriptorType::UniformBuffer) {
				Buffer* buffer = dynamic_cast<Buffer*>(
					write.buffer_info().buffer
				);
				glBindBufferRange(
					GL_UNIFORM_BUFFER,
					j,
					buffer->id(),
					write.buffer_info().offset,
					write.buffer_info().range
				);
			}
			else if (binding.type == DescriptorType::CombinedImageSampler) {
				Image* image = dynamic_cast<Image*>(
					write.image_info().image
				);
				glActiveTexture(GL_TEXTURE0 + binding.binding);
				glBindTexture(
					image->target(),
					image->id()
				);
			}
		}
	}
}

void Pipeline::draw(size_t vertices_count)
{
	glUseProgram(m_program_id);
	for (auto attrib : m_info.vertex_input.attributes) {
		glEnableVertexAttribArray(attrib.location);
	}

	glDrawElements(
		GL_TRIANGLES,
		vertices_count,
		m_index_type, 0
	);

	for (auto attrib : m_info.vertex_input.attributes) {
		glDisableVertexAttribArray(attrib.location);
	}

	glBindVertexArray(0);

	glUseProgram(0);
}

/* Shaders */

void Pipeline::load_stage(const char* data, GLint size, ShaderStage stage)
{
	GLenum type   = ShaderModule::get_gl_stage(stage.stage);
	GLuint shader = glCreateShader(type);

	// Compiling shader.
	glShaderSource(shader, 1, &data, &size);
	glCompileShader(shader);

	// Getting compilation status.
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	// Compilation failed.
	if (status == GL_FALSE) {
		// Storing logs in a buffer.
		GLint length;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
		std::vector<char> buffer(length);
		glGetShaderInfoLog(shader, length, nullptr, buffer.data());

		// Deleting the shader.
		glDeleteShader(shader);

		// Printing logs.
		throw std::runtime_error(buffer.data());
	}

	// If compilation went well, add this shader to the pipeline.
	m_stages_id.push_back(shader);
}

void Pipeline::link_shader()
{
	// Creating shader.
	m_program_id = glCreateProgram();

	// Attaching shaders.
	for (GLuint stage : m_stages_id)
		glAttachShader(m_program_id, stage);

	// Linking and validating program.
	glLinkProgram(m_program_id);
	glValidateProgram(m_program_id);
}

}}
