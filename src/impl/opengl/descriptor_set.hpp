#pragma once

#include "interface/descriptor_set.hpp"

namespace tgal {
namespace opengl {

struct DescriptorSet : public tgal::DescriptorSet {
	DescriptorSet(const CreateInfo& info)
		: m_info(info) {}

	const std::vector<tgal::DescriptorWrite>& writes() const
	{ return m_info.writes; }

private:
	CreateInfo m_info;
};

}}
