#include "physical_device.hpp"
#include "device.hpp"

namespace tgal {
namespace opengl {

tgal::Device* PhysicalDevice::create_device()
{
	return new Device();
}

}}
