#pragma once

#include <vector>

#include "interface/pipeline.hpp"
#include "interface/command_buffer.hpp"

#include "opengl.h"

namespace tgal {
namespace opengl {

class Device;
class Buffer;
class DescriptorSet;

class Pipeline : public tgal::Pipeline {
public:
	Pipeline(Device* device, const CreateInfo& info);
	~Pipeline();

	/* Drawing */

	void bind();
	void bind_index_buffer(Buffer* buffer, IndexType index_type);
	void bind_vertex_buffers(uint32_t first_binding, uint32_t binding_count, Buffer* buffer);
	void bind_descriptor_sets(
		uint32_t first_set, uint32_t count, DescriptorSet* sets);
	void draw(size_t vertices_count);

private:
	/* Shaders */

	void load_stage(const char* data, GLint size, ShaderStage stage);

	void link_shader();

private:
	CreateInfo m_info;
	unsigned m_vertex_array;
	GLenum m_index_type;

	// Shaders.
	std::vector<GLuint> m_stages_id;
	GLuint m_program_id = -1;
};

}}
