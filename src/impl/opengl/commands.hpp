#pragma once

#include "command_buffer.hpp"
#include "buffer.hpp"
#include "descriptor_set.hpp"

namespace tgal {
namespace opengl {

namespace Commands {

struct BeginRenderPass : public CommandBuffer::Command {
	BeginRenderPass(
		CommandBuffer* buffer,
		const RenderPass::BeginInfo& info
	) : p_buffer(buffer), m_info(info) {}

	virtual void execute() override;

private:
	CommandBuffer* p_buffer;
	RenderPass::BeginInfo m_info;
};


struct BindPipeline : public CommandBuffer::Command {
	BindPipeline(
		CommandBuffer* buffer,
		Pipeline* pipeline
	) : p_buffer(buffer), p_pipeline(pipeline) {}

	virtual void execute() override;

private:
	CommandBuffer* p_buffer;
	Pipeline* p_pipeline;
};


struct BindIndexBuffer : public CommandBuffer::Command {
	BindIndexBuffer(
		CommandBuffer* buffer,
		Buffer* index_buffer,
		IndexType type
	) :
		p_buffer(buffer),
		p_index_buffer(index_buffer),
		m_type(type)
	{}

	virtual void execute() override;

private:
	CommandBuffer* p_buffer;
	Buffer *p_index_buffer;
	IndexType m_type;
};


struct BindVertexBuffer : public CommandBuffer::Command {
	BindVertexBuffer(
		CommandBuffer* buffer,
		uint32_t first_binding,
		uint32_t bindings_count,
		Buffer* vertex_buffers
	) :
		p_buffer(buffer),
		m_first_binding(first_binding),
		m_bindings_count(bindings_count),
		p_vertex_buffers(vertex_buffers)
	{}

	virtual void execute() override;

private:
	CommandBuffer* p_buffer;
	uint32_t m_first_binding;
	uint32_t m_bindings_count;
	Buffer *p_vertex_buffers;
};


struct BindDescriptorSets : public CommandBuffer::Command {
	BindDescriptorSets(
		CommandBuffer* buffer,
		uint32_t first_set,
		size_t count,
		DescriptorSet* sets
	) :
		p_buffer(buffer),
		m_first_set(first_set),
		m_count(count),
		p_sets(sets)
	{}

	virtual void execute() override;

private:
	CommandBuffer* p_buffer;
	uint32_t m_first_set;
	size_t m_count;
	DescriptorSet* p_sets;
};


struct Draw : public CommandBuffer::Command {
	Draw(
		CommandBuffer* buffer,
		uint32_t vertex_count,
		uint32_t instance_count
	) :
		p_buffer(buffer),
		m_vertex_count(vertex_count),
		m_instance_count(instance_count)
	{}

	virtual void execute() override;

private:
	CommandBuffer* p_buffer;
	uint32_t m_vertex_count;
	uint32_t m_instance_count;
};


struct DrawIndexed : public CommandBuffer::Command {
	DrawIndexed(
		CommandBuffer* buffer,
		uint32_t index_count,
		uint32_t instance_count
	) :
		p_buffer(buffer),
		m_index_count(index_count),
		m_instance_count(instance_count)
	{}

	virtual void execute() override;

private:
	CommandBuffer* p_buffer;
	uint32_t m_index_count;
	uint32_t m_instance_count;
};

}}}
