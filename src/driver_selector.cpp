#include <iostream>
#include <stdexcept>

#include "driver_selector.hpp"

#include "impl/vulkan/instance.hpp"
#include "impl/vulkan/window.hpp"

#include "impl/opengl/instance.hpp"
#include "impl/opengl/window.hpp"

namespace tgal {

DriverSelector::DriverSelector(DriverType type)
	: m_type(type)
{
}

void DriverSelector::set_driver(DriverType type)
{
	m_type = type;
}

Window* DriverSelector::create_window(
	const Window::CreateInfo& info) const
{
	switch (m_type) {
		case DriverType::Vulkan:
			return new tgal::vulkan::Window(info);
		case DriverType::OpenGL:
			return new tgal::opengl::Window(info);
	}

	throw std::runtime_error("Unknown backend selected.");
}

Instance* DriverSelector::create_instance(
	const Instance::CreateInfo& info) const
{
	switch (m_type) {
		case DriverType::Vulkan:
			return new tgal::vulkan::Instance(info);
		case DriverType::OpenGL:
			return new tgal::opengl::Instance(info);
	}

	throw std::runtime_error("Unknown backend selected.");
}

}
