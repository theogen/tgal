#pragma once

#include "interface/instance.hpp"
#include "interface/window.hpp"

namespace tgal {

enum class DriverType {
	Vulkan,
	OpenGL
};

class DriverSelector {
public:
	DriverSelector(DriverType type);

	void set_driver(DriverType type);

	Window* create_window(const Window::CreateInfo& info) const;
	Instance* create_instance(const Instance::CreateInfo& info) const;

private:
	DriverType m_type;
};

}
