#pragma once

namespace tgal {

// Semantic version representation.
// Initializer list isn't used because major and minor can be predefined.
struct Version {
	int major;
	int minor;
	int patch;

	Version() {}
	Version(int major, int minor, int patch)
	{
		this->major = major;
		this->minor = minor;
		this->patch = patch;
	}
};

}
