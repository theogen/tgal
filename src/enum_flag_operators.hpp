#pragma once

#include <cstdint>

// Define an operator for an enum type.
#define DEFINE_ENUM_OPERATOR(TYPE, OPERATOR) \
	constexpr TYPE operator OPERATOR (const TYPE a, const TYPE b) \
	{ \
		return static_cast<TYPE>(static_cast<uint32_t>(a) OPERATOR static_cast<uint32_t>(b)); \
	}

// Define flag operators for an enum type.
#define DEFINE_ENUM_FLAG_OPERATORS(TYPE) \
	DEFINE_ENUM_OPERATOR(TYPE, |) \
	DEFINE_ENUM_OPERATOR(TYPE, &) \
	DEFINE_ENUM_OPERATOR(TYPE, >>) \
	DEFINE_ENUM_OPERATOR(TYPE, <<)
