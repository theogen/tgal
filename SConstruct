# vim: ft=python

import os
import shutil
import glob
import shutil 

# Program/library name.
progname = 'tgal'

# SCons environment.
env = Environment()

#########################################################################
# OPTIONS ###############################################################
#########################################################################

AddOption(
    '--progname',
    dest='progname',
    nargs=1, type='string',
    action='store',
    metavar='name',
    default=progname,
    help='Build & install program by given name.'
)
progname = GetOption('progname')

AddOption(
    '--prefix',
    dest='prefix',
    nargs=1, type='string',
    action='store',
    metavar='DIR',
    default='install',
    help='Installation prefix.'
)

AddOption(
    '--build-prefix',
    dest='buildpfx',
    nargs=1, type='string',
    action='store',
    metavar='DIR',
    default='build',
    help='Build prefix.'
)

AddOption(
    '--verbose',
    dest='verbose',
    action='store_true',
    default=False,
    help='Verbose output.'
)

#########################################################################
# VERBOSITY #############################################################
#########################################################################

if not GetOption('verbose'):
    env.Append(
        CXXCOMSTR = "\033[32mCompiling `\033[39;1m$TARGET\033[32;21;24m'\033[0m",
        LINKCOMSTR = "\033[36mLinking `\033[39;1m$TARGET\033[36;21;24m'\033[0m",
        ARCOMSTR = "\033[32mArchiving `\033[39;1m$TARGET\033[32;21;24m'\033[0m",
        RANLIBCOMSTR = "\033[32mMaking library `\033[39;1m$TARGET\033[32;21;24m'\033[0m",
        INSTALLSTR = "\033[34mInstall `\033[39;1m$TARGET\033[34;21;24m'\033[0m",
        CLEANSTR = "\033[31mRemove `\033[39;1m$TARGET\033[31;21;24m'\033[0m"
    )


#########################################################################
# LIBRARIES #############################################################
#########################################################################

# Libraries.
env.Append(LIBS = [
    'SDL2',
    'SDL2_image',
    'GL',
    'GLEW',
    'vulkan',
    'glslang',
    'SPIRV'
])

#########################################################################
# BUILD TYPE ############################################################
#########################################################################

# Build type.
if ARGUMENTS.get('build') != 'release':
    build_type = 'debug'
else:
    build_type = 'release'


#########################################################################
# DIRECTORIES ###########################################################
#########################################################################

src_dir = 'src'
lib_dir = 'lib'

# Build prefix.
build_pfx = GetOption('buildpfx')

# Adding build type.
build_dir = build_pfx + '/' + build_type

build_lib_dir = build_dir + '/lib'
build_obj_dir = build_dir + '/obj'

target_path = build_lib_dir + '/' + progname

# Installation prefix.
install_pfx = GetOption('prefix')

install_inc_dir = install_pfx + '/include/' + progname
install_lib_dir = install_pfx + '/lib'

env.VariantDir(build_obj_dir, src_dir, duplicate=0)

# Including all subdirectories recursively.
sources = []
headers = []
for dir in [x[0] for x in os.walk(src_dir)]:
    headers += Glob(dir + '/*.hpp')
    headers += Glob(dir + '/*.h')
    wo_basedir = dir[len(src_dir):]
    if len(wo_basedir) == 0:
        wo_basedir = '/'
    sources += Glob(build_obj_dir + '/' + wo_basedir + '/*.cpp')


#########################################################################
# FLAGS #################################################################
#########################################################################

# Show all warnings.
flags  = ' -Wall'

# Build type.
if build_type == 'debug':
    flags += ' -g -rdynamic'
else:
    flags += ' -O3 -DNDEBUG'

# C11 standard.
flags += ' -std=c11'

# Source directory.
flags += ' -I' + src_dir

# Libraries directory.
flags += ' -isystem' + lib_dir

# Colored output.
flags += ' -fdiagnostics-color'


#########################################################################
# BUILD #################################################################
#########################################################################

if ARGUMENTS.get('shared') == '1':
    # Make shaded library.
    target = env.SharedLibrary(target_path, sources, parse_flags = flags)
else:
    # Make static library.
    target = env.StaticLibrary(target_path, sources, parse_flags = flags)


#########################################################################
# INSTALL ###############################################################
#########################################################################

install = 'install' in COMMAND_LINE_TARGETS
install_action = None

if install:
    for header in headers:
        dir = os.path.dirname(header.path)[len(src_dir):]
        dest = install_inc_dir + dir
        env.Install(dest, header)
        env.Alias('install', dest)

    env.Install(install_lib_dir, target)
    install_action = env.Alias('install', install_lib_dir)


#########################################################################
# CLEAN #################################################################
#########################################################################

def rm_empty_dirs(path, rmroot=True):
    'Function to remove empty folders'
    if not os.path.isdir(path):
        return

    # remove empty subfolders
    files = os.listdir(path)
    if len(files):
        for f in files:
            fullpath = os.path.join(path, f)
            if os.path.isdir(fullpath):
                rm_empty_dirs(fullpath)

    # if folder empty, delete it
    files = os.listdir(path)
    if len(files) == 0 and rmroot:
        print('Remove empty directory: %s' % path)
        os.rmdir(path)

# Remove empty directories recursively.
# TODO: You need to call `scons -c` twice for it to work.
if GetOption('clean'):
    rm_empty_dirs(build_pfx)
    rm_empty_dirs(install_pfx)
