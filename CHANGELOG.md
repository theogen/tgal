# Changelog

[kacl]: http://keepachangelog.com/en/1.0.0/
[semver]: http://semver.org/spec/v2.0.0.html

All notable changes to this project will be documented in this file.

The changelog format is based on [Keep a Changelog][kacl], and this project
adheres to [Semantic Versioning][semver].

## [TODO]
### Add
- Automatic cleanup.
- Proper descriptor set allocation.
- Different command buffer for single time commands.
- Different queue for commands.
- Choose image format correctly when loading an image from a file.
- Shader caching.


## [Unreleased]
### Add
- Remove test renderer and all assets. This repository now only contains the
  library.
- Rename namespaces `ngal_impl` and `ngal` to `tgal`.
- Support different build types and installation.


## [0.3.0] - 2019-10-02
### Add
- OpenGL support.
- Automatic swapchain recreation.
- Commands re-recording.


## [0.2.0] - 2019-07-26
### Add
- Expose SDL window flags.
- Depth buffering support.
- Models loading.
- Expose framebuffers.
- Create descriptor sets directly.
- Mipmaps support.
- A single header for interfaces.
- A single buffer write method instead of mapping and unmapping.
- Shared library construction.


## [0.1.0] - 2019-06-19
### Add
- An advanced memory allocator.
- API agnostic format.
