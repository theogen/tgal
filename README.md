# README

*tgal* - Theo's Graphics Abstraction Library. The title is quite
self-explanatory. It provides a single interface for different graphics
backends: Vulkan and OpenGL in particluar. But other backends can be easily
implemented if needed. It uses *SDL 2* for window management.

## Building

You need *SConstruct* to compile the library. The following command should
suffice:

`scons shared=1`

You can also specify jobs count using the `-j` option.

## Demo

![Screenshot](screenshot.jpg)

House from [vulkan-tutorial.com](https://vulkan-tutorial.com/) rendered by tgal.
